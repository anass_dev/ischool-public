<?php

namespace App\Http\Controllers;

use App\Course;
use App\Group;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CourseController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group)
    {
        return view('groups.coures.index',compact($group->courses));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Group $group)
    {
        $course = Course::make($this->validateRequest());
        $course->group_id = $group->id;
        $course->save();

        return redirect('groups/'.$group->id.'/courses');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Group $group, Course $course)
    {
        $course->update($this->validateRequest());

        return redirect('groups/'.$group->id.'/courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group,Course $course)
    {
       $course->delete();
    }

    /**
     * 
     * 
     * 
     */
     private function validateRequest(){

        return request()->validate([

                'classroom_id'  => 'required|integer|exists:App\Classroom,id',
                'subject_id'    => 'required|integer|exists:App\Subject,id',
                'teacher_id'    => 'required|integer|exists:App\Teacher,id',
                'day' => [
                    'required',
                    Rule::in(['monday', 'tuesday', 'wednesday', 'thursday', 'friday','saturday','sunday']),
                ],
                'from'          => 'required|regex:/[0-9]{2}h[0-9]{2}/',
                'to'            => 'required|regex:/[0-9]{2}h[0-9]{2}/',
        ]);

     }
}
