<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complaint;
use App\Student;

class ComplaintController extends Controller
{
    /**
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $student)
    {
        return view('students.complaints.index')->with(['complaints'=>$student->complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $student)
    {
        return view('students.complaints.create')->with(['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $Student)
    {
        $complaint  = Complaint::make($this->validateRequest());
        $complaint->student_id = $Student->id;
        $complaint->save();

        return redirect('/students/'.$Student->id.'/complaints');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,Complaint $complaint)
    {
        return view('students.complaints.edit')->with(['complaint'=>$complaint,"student"=>$student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Student $Student,Complaint $complaint)
    {
        $complaint->update($this->validateRequest());

        return redirect('/students/'.$Student->id.'/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $Student,Complaint $complaint)
    {
        $complaint->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'          => 'required|max:100',
            'description'   => 'nullable',
        ]);

    } 
}
