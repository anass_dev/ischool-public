<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Group;
use App\User;
use App\Grade;

class GroupControllerTest extends TestCase
{
    
    
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_group(){

        $response = $this->get('/groups')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_group(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/groups')->assertOk();

    }




    /** @test */
    public function show_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/groups/create')->assertOk();

    }




    /** @test */
    public function group_view_create_is_accssible(){

       

        $this->actingAs(Factory(User::class)->create());
        $group = factory(Group::class)->create();

        $response = $this->get('/groups/'.$group->id)->assertOk();

    }




    /** @test */
    public function group_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        
        $this->post('/groups',[

            'name'          =>  'groupname',
            'grade_id'      => factory(Grade::class)->create()->id
        ]);

        $this->assertCount(1,Group::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/groups',[
            'name'          =>  '',
            'grade_id'      => factory(Grade::class)->create()->id
        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_group(){

       $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $group = factory(Group::class)->create();

        $response = $this->get('/groups/'.$group->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_group_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $group = Factory(Group::class)->create();
    
        $this->patch('/groups/'.$group->id,[

                'name'          => 'new_name',
                'grade_id'      => factory(Grade::class)->create()->id
    
        ]);

        $this->assertDatabaseHas('groups', ['name'=>'new_name']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $group = Factory(Group::class)->create();
    
        $this->patch('/groups/'.$group->id,[

                'name'          => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('groups', ['name'=>'']);

    
    }




    /** @test */
    public function a_group_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $group = Factory(Group::class)->create();

    
        $this->delete('/groups/'.$group->id);
        $this->assertSoftDeleted('groups', ['id'=>$group->id]);
        
        
    }
}
