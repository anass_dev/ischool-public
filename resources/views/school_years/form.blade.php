<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Name') }}</label>
                    <am-input name="name" type="text" value="{{ $school_year->name ?? old('name') }}"  ></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


    <div class="flex flex-wrap">


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="first_day" class="text-md">{{ _('First day') }}</label>
                    <am-input name="first_day" type="date" value="{{ $school_year->first_day ?? old('first_day') }}"  ></am-input>
                    @error('first_day')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="last_day" class="text-md">{{ _('Last day') }}</label>
                    <am-input name="last_day" type="date" value="{{ $school_year->last_day ?? old('last_day') }}"  ></am-input>
                    @error('last_day')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


</div>