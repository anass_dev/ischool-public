@extends('layouts.app')
@section('content')

<div class="p-2">
    <!-- ===== Breadcrumb ===== -->
            
    <breadcrumb :items="[

        {'name':'providers', 'link':'{{ route('providers.index') }}' }, 
        {'name':'edit', 'link':'' }

    ]"></breadcrumb>

</div>

<div class="container p-2">       


    <div class="border shadow-sm rounded-md">


        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Edit Provider') }}"></form-header>
        </div>  
        <form action="{{ route('providers.update',$provider) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                        @method('PUT')
                        @include('providers.form')
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('providers.index') }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>
        </form>

    </div>
</div>





@endsection