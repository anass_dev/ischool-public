<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeSalary as Salary;

use Illuminate\Http\Request;

class EmployeeSalaryController extends Controller
{
    
    /**
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.salaries.index')->with(['salaries'=>Salary::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee)
    {
        return view('employees.salaries.create')->with(['employee'=>$employee]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Employee $employee)
    {
        $salary = Salary::make($this->validateRequest());
        $salary->employee_id = $employee->id;
        $salary->save();

        return redirect('employees/'.$employee->id.'/salaries');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee,Salary $salary)
    {
        view('employees.salaries.edit')->with(['employee'=>$employee,"salary"=>$salary]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee,Salary $salary)
    {
        $salary->update($this->validateRequest());

        return redirect('employees/'.$employee->id.'/salaries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee,Salary $salary)
    {
        $salary->delete();
        return redirect('employees/'.$employee->id.'/salaries');
    }

    /**
     * 
     * 
     */
     private function validateRequest(){

        return request()->validate([
            'amount'        => 'required|numeric'
        ]);

     }
}
