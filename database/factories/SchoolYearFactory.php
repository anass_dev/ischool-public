<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SchoolYear;
use Faker\Generator as Faker;


$factory->define(SchoolYear::class, function (Faker $faker) {
    return [
        

        'name'          =>  '2018', 
        'first_day'     =>  $faker->dateTimeBetween('-2 years -9 months'),
        'last_day'      =>  $faker->dateTimeBetween('-1 years'),

    ];
});
