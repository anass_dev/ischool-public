<?php

namespace Tests\Feature;

use App\User;
use App\Course;
use App\Group;
use App\Subject;
use App\Teacher;
use App\Classroom;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;



class CourseControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_course(){

        $group = factory(Group::class)->create();

        $this->get('groups/'.$group->id.'/courses')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_course(){

    
        $this->actingAs(Factory(User::class)->create());

        $group = factory(Group::class)->create();
        $this->get('groups/'.$group->id.'/courses')->assertOk();

    }




    /** @test */
    public function show_view_is_not_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $group = factory(Group::class)->create();
        $this->get('groups/'.$group->id.'/courses/create')->assertStatus(405);

    }




    /** @test */
    public function course_view_is_not_accssible(){

       

        $this->actingAs(Factory(User::class)->create());

        $course = factory(Course::class)->create();
        $group  = factory(Group::class)->create();

        $this->get('groups/'.$group->id.'/courses/'.$course->id)->assertStatus(405);

    }




    /** @test */
    public function course_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $group  = factory(Group::class)->create();

        
        $this->post('groups/'.$group->id.'/courses',[
 
                'classroom_id'      => factory(Classroom::class)->create()->id,
                'subject_id'        => factory(Subject::class)->create()->id,
                'teacher_id'        => factory(Teacher::class)->create()->id,
                'day'               => 'monday',
                'from'              => '15h00',
                'to'                => '16h00'

        ]);



        $this->assertCount(1,Course::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $group  = factory(Group::class)->create();

        $this->post('groups/'.$group->id.'/courses',[

            'classroom_id'      => factory(Classroom::class)->create()->id,
            'subject_id'        => factory(Subject::class)->create()->id,
            'teacher_id'        => factory(Teacher::class)->create()->id,
            'day'               => 'dsqqqsd',
            'from'              => '15dsh00',
            'to'                => '16hd00'


        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function course_view_update_is_not_accssible(){

       

        $this->actingAs(Factory(User::class)->create());

        $course = factory(Course::class)->create();
        $group  = factory(Group::class)->create();



        $response = $this->get('groups/'.$group->id.'/courses/'.$course->id.'/edit');

        $response->assertStatus(404);

    }




    /** @test */
    public function a_course_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $course = Factory(Course::class)->create();
        $group  = factory(Group::class)->create();

        $class   = factory(Classroom::class)->create();
        $subject = factory(Subject::class)->create();
        $teacher = factory(Teacher::class)->create();


        
        $this->patch('groups/'.$group->id.'/courses/'.$course->id,[

            'classroom_id'      => $class->id,
            'subject_id'        => $subject->id,
            'teacher_id'        => $teacher->id,
            'day'               => 'monday',
            'from'              => '15h00',
            'to'                => '16h00'
        ]);



        $this->assertDatabaseHas('courses', [
            'classroom_id'      => $class->id,
            'subject_id'        => $subject->id,
            'teacher_id'        => $teacher->id,
            'day'               => 'monday',
            'from'              => '15h00',
            'to'                => '16h00'
        ]);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $course = Factory(Course::class)->create();
        $group  = factory(Group::class)->create();

        $this->patch('groups/'.$group->id.'/courses/'.$course->id,[

            'classroom_id'      => false,
            'subject_id'        => '',
            'teacher_id'        => '',
            'day'               => '',
            'from'              => '15sdh00',
            'to'                => '16hsdq00'
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('courses', [
            
            'classroom_id'      => false,
            'subject_id'        => '',
            'teacher_id'        => '',
            'day'               => '',
            'from'              => '15sdh00',
            'to'                => '16hsdq00'

        ]);

    
    }




    /** @test */
    public function a_course_can_be_deleted(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $course = Factory(Course::class)->create();
        $group  = factory(Group::class)->create();

    
        $this->delete('groups/'.$group->id.'/courses/'.$course->id);
        $this->assertSoftDeleted('courses', ['id'=>$course->id]);
        
        
    }
}
