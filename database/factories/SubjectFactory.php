<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subject;
use Faker\Generator as Faker;

$factory->define(Subject::class, function (Faker $faker) {
    return [
        
        'name'          => $faker->word(),
        'standard'          => true,
        'description'   => $faker->text(100),
        'hours_number'  =>  $faker->numberBetween(40, 150),
    ];
});
