@extends('layouts.app')
@section('content')

<div class="pt-2 px-4">
                    <!-- ===== Breadcrumb ===== -->
                
                    <breadcrumb :items="[

                        {'name':'employees', 'link':'{{ route('employees.index') }}' }, 
                        {'name':'{{ $employee->name . ' ' . $employee->surname }}', 'link':'' }
                    
            ]"></breadcrumb>
</div>

<div class="container p-2">
    

    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-5 rounded-md shadow-md">

                
                <div class="mt-2 flex flex-col items-center justify-center">
                    <div class="text-center">
                        <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26">
                            <path d="M23.995 24h-1.995c0-3.104.119-3.55-1.761-3.986-2.877-.664-5.594-1.291-6.584-3.458-.361-.791-.601-2.095.31-3.814 2.042-3.857 2.554-7.165 1.403-9.076-1.341-2.229-5.413-2.241-6.766.034-1.154 1.937-.635 5.227 1.424 9.025.93 1.712.697 3.02.338 3.815-.982 2.178-3.675 2.799-6.525 3.456-1.964.454-1.839.87-1.839 4.004h-1.995l-.005-1.241c0-2.52.199-3.975 3.178-4.663 3.365-.777 6.688-1.473 5.09-4.418-4.733-8.729-1.35-13.678 3.732-13.678 4.983 0 8.451 4.766 3.732 13.678-1.551 2.928 1.65 3.624 5.09 4.418 2.979.688 3.178 2.143 3.178 4.663l-.005 1.241zm-13.478-6l.91 2h1.164l.92-2h-2.994zm2.995 6l-.704-3h-1.615l-.704 3h3.023z"/>
                        </svg>
                    </div>

                    <h2 class="p-2 text-xl text-center text-gray-700">{{ $employee->name . ' ' . $employee->surname }}</h2>
                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-address-card"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $employee->cin }}</div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-phone"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $employee->phone_number }}</div>

                </div>

                <div class="mt-2 flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-at"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $employee->email }} </div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-calendar"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $employee->birthday }} </div>

                </div>

                <div class="flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-map"></i>  </label>
                    <textarea class="p-2 text-sm text-gray-700" cols="30" rows="3" readonly>{{ $employee->address }}</textarea>
                </div>
            </div>

            </div>

            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">
                <div class="m-2 p-5 rounded-md shadow-md">
                    <div class="flex items-center justify-between">

                    <div class="flex items-center justify-start ">

                        <svg class="fill-current text-gray-400 w-8 mx-2 mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                            <path d="M12.164 7.165c-1.15.191-1.702 1.233-1.231 2.328.498 1.155 1.921 1.895 3.094 1.603 1.039-.257 1.519-1.252 1.069-2.295-.471-1.095-1.784-1.827-2.932-1.636zm1.484 2.998l.104.229-.219.045-.097-.219c-.226.041-.482.035-.719-.027l-.065-.387c.195.03.438.058.623.02l.125-.041c.221-.109.152-.387-.176-.453-.245-.054-.893-.014-1.135-.552-.136-.304-.035-.621.356-.766l-.108-.239.217-.045.104.229c.159-.026.345-.036.563-.017l.087.383c-.17-.021-.353-.041-.512-.008l-.06.016c-.309.082-.21.375.064.446.453.105.994.139 1.208.612.173.385-.028.648-.36.774zm10.312 1.057l-3.766-8.22c-6.178 4.004-13.007-.318-17.951 4.454l3.765 8.22c5.298-4.492 12.519-.238 17.952-4.454zm-2.803-1.852c-.375.521-.653 1.117-.819 1.741-3.593 1.094-7.891-.201-12.018 1.241-.667-.354-1.503-.576-2.189-.556l-1.135-2.487c.432-.525.772-1.325.918-2.094 3.399-1.226 7.652.155 12.198-1.401.521.346 1.13.597 1.73.721l1.315 2.835zm2.843 5.642c-6.857 3.941-12.399-1.424-19.5 5.99l-4.5-9.97 1.402-1.463 3.807 8.406-.002.007c7.445-5.595 11.195-1.176 18.109-4.563.294.648.565 1.332.684 1.593z"/>
                        </svg>

                        <h2 class="text-gray-600 text-xl py-5"><a href="{{ route('employees.salaries.index', ['employee' => $employee->id]) }}"> Salary History </a></h2>
                        
                    </div>
                        <div>
                            <success-button text="{{ _('Add salary') }}" link="{{ route('employees.salaries.create', ['employee' => $employee->id]) }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $employee->salaries()->get(['id','amount','created_at']) }}" 
                        index_link="{{ route('employees.salaries.index',['employee'=>$employee]) }}" ></small-table>

                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection