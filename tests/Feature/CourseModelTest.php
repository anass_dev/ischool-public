<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Tests\TestCase;
use App;

class CourseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 
     * tests if the subject correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_course_is_saved(){

        
        $Course = factory(App\Course::class)->create();

        $Course->save();

        $this->assertDatabaseHas('courses', ['id'=>$Course->id]);


    }   




    /**
     * 
     * tests if the subject correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_course_is_updated(){
        
        $Course = factory(App\Course::class)->create();
        
        $Course->update(['from' => '20:50']);
        
        $this->assertDatabaseHas('courses', ['from' => '20:50']);

    }




    /**
     * 
     * tests if the subject correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_course_is_softDeleted(){

        $Course = factory(App\Course::class)->create();

        $Course->delete();

        $this->assertSoftDeleted($Course);

    }




    /**
     * 
     * tests if the subject correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_course_founde(){

        $course = factory(App\Course::class)->create();
        $course->save();


        $new_course = App\Course::find($course->id);

        $this->assertEquals($course->id,$new_course->id);

    }
}
