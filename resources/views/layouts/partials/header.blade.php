        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--==== CSRF Token ====-->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'iSchool') }}</title>

        <!--==== Fonts ====-->
        <link   rel="dns-prefetch" href="//fonts.gstatic.com">
        <link   href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" 
                integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" 
                crossorigin="anonymous"></script>

        <!--==== Styles ====-->
        <link rel="stylesheet" href="{{ url('css/sweetalert2.min.css') }}">
        <link   href="{{ mix('css/app.css') }}" rel="stylesheet">

        <!-- ==== favicons ==== -->
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/site.webmanifest">
