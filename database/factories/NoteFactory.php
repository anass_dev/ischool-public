<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Note;
use Faker\Generator as Faker;

$factory->define(Note::class, function (Faker $faker) {
    return [
        
        'name'          =>  $faker->word(),
        'description'   =>   $faker->text(50),
        'student_id'    => factory(App\Student::class)->create()
      
    ];
});
