<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Provider;
use Faker\Generator as Faker;

use Illuminate\Support\Str;


$factory->define(Provider::class, function (Faker $faker) {
    return [
        'name'          => $faker->company(), 
        'phone_number'  => $faker->e164PhoneNumber(),

        'email'         =>  $faker->safeEmail(),
        'address'       =>  $faker->address()
    ];
});
