@component('mail::message')

Hi {{ $parent->name }} {{ $parent->surname }},

we notify you that {{ $delay->student->name }} {{ $delay->student->surname }} was delayed for {{ $delay->duration }}mins.

@if(trim($delay->description) != '')
more information:
{{ $delay->description }}
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent