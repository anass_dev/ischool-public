@extends('layouts.app')
@section('content')

    
    <!-- ===== Breadcrumb ===== -->

    <div class="pt-2 px-4">
         
        <breadcrumb :items="[

                {'name':'students', 'link':'{{ route('students.index') }}' }, 
                {'name':'{{ $student->name . ' ' . $student->surname }}', 'link':'' }
            
        ]"></breadcrumb>
   
   </div>

   <!-- ===== End Breadcrumb ===== -->

<div class="container p-2">
    
    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-4 rounded-md shadow-md">

                
                <div class="mt-2 flex flex-col items-center justify-center">
                    <div class="text-center">
                        
                        <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26">
                            <path d="M16.5 14.5c0 .828-.56 1.5-1.25 1.5s-1.25-.672-1.25-1.5.56-1.5 1.25-1.5 1.25.672 1.25 1.5zm-7.75-1.5c-.69 0-1.25.672-1.25 1.5s.56 1.5 1.25 1.5 1.25-.672 1.25-1.5-.56-1.5-1.25-1.5zm3.25 8.354c2.235 0 3-2.354 3-2.354h-6s.847 2.354 3 2.354zm12-6.041c0 1.765-.985 3.991-3.138 4.906-2.025 3.233-4.824 5.781-8.862 5.781-3.826 0-6.837-2.548-8.862-5.781-2.153-.916-3.138-3.142-3.138-4.906 0-2.053.862-3.8 2.71-3.964.852-9.099 8.57-8.408 9.837-10.849.323.559.477 1.571-.02 2.286.873-.045 2.344-1.304 2.755-2.552.754.366 1.033 1.577.656 2.354.542-.103 2.187-1.15 3.062-2.588.688 1.563.026 3.563-.708 4.771l-.012.001c1.796 1.707 2.781 4.129 3.01 6.576 1.859.165 2.71 1.917 2.71 3.965zm-2.58-1.866c-.235-.152-.531-.115-.672-.053-.56.25-1.214-.062-1.372-.66l-.001.016c-.333-2.604-1.125-4.854-2.611-5.565-6.427 7.009-10.82-.914-11.94 3.529-.101.582-.166 1.172-.166 1.766 0 .719-.743 1.209-1.406.914-.14-.062-.437-.1-.672.053-1 .651-.894 4.184 1.554 5.012.224.076.413.228.535.43 2.447 4.053 5.225 5.111 7.331 5.111 3.288 0 5.615-2.269 7.332-5.111.122-.202.312-.354.535-.43 2.447-.828 2.553-4.361 1.553-5.012z"/>
                        </svg>
                    </div>

                    <h2 class="p-2 text-xl text-center text-gray-700">{{ $student->name . ' ' . $student->surname }}</h2>
                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-address-card"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $student->cin }}</div>
                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-phone"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $student->phone_number }}</div>

                </div>

                <div class="mt-2 flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-at"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $student->email }} </div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-calendar"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $student->birthday }} </div>
                </div>

                <div class="flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-map"></i>  </label>
                    <textarea class="p-2 text-sm text-gray-700" cols="30" rows="3" readonly>{{ $student->address }}</textarea>
                </div>
            </div>

            </div>

            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">


                <div class="m-2 p-4 border rounded-md shadow-md">
                    <div class="flex items-center justify-between">
                        <div class="flex items-center justify-start">
                            <svg class="fill-current text-gray-400 w-8 mx-2 mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                <path d="M12.164 7.165c-1.15.191-1.702 1.233-1.231 2.328.498 1.155 1.921 1.895 3.094 1.603 1.039-.257 1.519-1.252 1.069-2.295-.471-1.095-1.784-1.827-2.932-1.636zm1.484 2.998l.104.229-.219.045-.097-.219c-.226.041-.482.035-.719-.027l-.065-.387c.195.03.438.058.623.02l.125-.041c.221-.109.152-.387-.176-.453-.245-.054-.893-.014-1.135-.552-.136-.304-.035-.621.356-.766l-.108-.239.217-.045.104.229c.159-.026.345-.036.563-.017l.087.383c-.17-.021-.353-.041-.512-.008l-.06.016c-.309.082-.21.375.064.446.453.105.994.139 1.208.612.173.385-.028.648-.36.774zm10.312 1.057l-3.766-8.22c-6.178 4.004-13.007-.318-17.951 4.454l3.765 8.22c5.298-4.492 12.519-.238 17.952-4.454zm-2.803-1.852c-.375.521-.653 1.117-.819 1.741-3.593 1.094-7.891-.201-12.018 1.241-.667-.354-1.503-.576-2.189-.556l-1.135-2.487c.432-.525.772-1.325.918-2.094 3.399-1.226 7.652.155 12.198-1.401.521.346 1.13.597 1.73.721l1.315 2.835zm2.843 5.642c-6.857 3.941-12.399-1.424-19.5 5.99l-4.5-9.97 1.402-1.463 3.807 8.406-.002.007c7.445-5.595 11.195-1.176 18.109-4.563.294.648.565 1.332.684 1.593z"/>
                            </svg>
                         <h2 class="text-gray-600 text-xl py-2 w-1/2"><a href="{{ route('students.paiments.index', ['student' => $student->id]) }}"> Payments </a></h2>
                        </div>
                         <div>
                            <success-button text="{{ _('Add paiment') }}" link="{{ route('students.paiments.create', ['student' => $student->id]) }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $student->paiments()->get(['id','name','amount','created_at']) }}" 
                        index_link="{{ route('students.paiments.index', ['student' => $student->id]) }}" ></small-table>
                </div>
                <div class="m-2 p-4 border rounded-md shadow-md">
                        <div class="flex items-center justify-between">
                            <div class="flex items-center justify-start">
                                <svg class="fill-current text-gray-400 w-8 mx-2 mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                    <path d="M21.5 6c.276 0 .5.224.5.5v11c0 .276-.224.5-.5.5h-19c-.276 0-.5-.224-.5-.5v-11c0-.276.224-.5.5-.5h19zm2.5 0c0-1.104-.896-2-2-2h-20c-1.104 0-2 .896-2 2v12c0 1.104.896 2 2 2h20c1.104 0 2-.896 2-2v-12zm-20 3.78c0-.431.349-.78.78-.78h.427v1.125h-1.207v-.345zm0 .764h1.208v.968h-1.208v-.968zm0 1.388h1.208v1.068h-.428c-.431 0-.78-.349-.78-.78v-.288zm4 .288c0 .431-.349.78-.78.78h-.429v-1.068h1.209v.288zm0-.708h-1.209v-.968h1.209v.968zm0-1.387h-1.629v2.875h-.744v-4h1.593c.431 0 .78.349.78.78v.345zm5.5 2.875c-1.381 0-2.5-1.119-2.5-2.5s1.119-2.5 2.5-2.5c.484 0 .937.138 1.32.377-.53.552-.856 1.3-.856 2.123 0 .824.326 1.571.856 2.123-.383.239-.836.377-1.32.377zm1.5-2.5c0-1.381 1.12-2.5 2.5-2.5 1.381 0 2.5 1.119 2.5 2.5s-1.119 2.5-2.5 2.5c-1.38 0-2.5-1.119-2.5-2.5zm-8 4.5h-3v1h3v-1zm4 0h-3v1h3v-1zm5 0h-3v1h3v-1zm4 0h-3v1h3v-1z"/>                                </svg>
                            <h2 class="text-gray-600 text-xl py-2 w-1/2"> <a href="{{ route('students.debts.index', ['student' => $student->id]) }}">Debts </a></h2>
                            </div>
                            <div>
                                <success-button text="{{ _('Add Debt') }}" link="{{ route('students.debts.create', ['student' => $student->id]) }}"></success-button>
                            </div>
                        </div>

                        <small-table 
                            :items="{{ $student->debts()->get(['id','name','created_at']) }}" 
                            index_link="{{ route('students.debts.index', ['student' => $student->id]) }}"></small-table>
                </div>
                <div class="m-2 p-4 border rounded-md shadow-md">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center justify-start">
                                    <svg class="fill-current text-gray-400 w-8 mx-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                        <path d="M17 1c0-.552-.447-1-1-1s-1 .448-1 1v2c0 .552.447 1 1 1s1-.448 1-1v-2zm-12 2c0 .552-.447 1-1 1s-1-.448-1-1v-2c0-.552.447-1 1-1s1 .448 1 1v2zm13 5v10h-16v-10h16zm2-6h-2v1c0 1.103-.897 2-2 2s-2-.897-2-2v-1h-8v1c0 1.103-.897 2-2 2s-2-.897-2-2v-1h-2v18h20v-18zm-11.646 14c-1.318 0-2.192-.761-2.168-2.205h1.245c.022.64.28 1.107.907 1.107.415 0 .832-.247.832-.799 0-.7-.485-.751-1.3-.751v-.977c.573.05 1.196-.032 1.196-.608 0-.455-.369-.663-.711-.663-.575 0-.793.422-.782 1.003h-1.256c.052-1.401.902-2.107 2.029-2.107.968 0 1.969.613 1.969 1.64 0 .532-.234.945-.638 1.147.528.203.847.681.847 1.293-.001 1.201-.993 1.92-2.17 1.92zm5.46 0h-1.306v-3.748h-1.413v-1.027c.897.024 1.525-.233 1.657-1.113h1.062v5.888zm10.186-11v19h-22v-2h20v-17h2z"/>                                    </svg>
                                <h2 class="text-gray-600 text-xl py-2 w-1/2"> <a href="{{ route('students.absents.index', ['student' => $student->id]) }}">Absents </a></h2>
                                </div>
                                <div>
                                    <success-button text="{{ _('Add Absent') }}" link="{{ route('students.absents.create', ['student' => $student->id]) }}"></success-button>
                                </div>
                            </div>
                        <small-table 
                            :items="{{ $student->absents()->get(['id','name','created_at']) }}" 
                            index_link="{{ route('students.absents.index', ['student' => $student->id]) }}"></small-table>
                </div>    
                <div class="m-2 p-4 border rounded-md shadow-md">
                        <div class="flex items-center justify-between">
                            <div class="flex items-center justify-start">
                                <svg class="fill-current text-gray-400 w-8 mx-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                    <path d="M15.91 13.34l2.636-4.026-.454-.406-3.673 3.099c-.675-.138-1.402.068-1.894.618-.736.823-.665 2.088.159 2.824.824.736 2.088.665 2.824-.159.492-.55.615-1.295.402-1.95zm-3.91-10.646v-2.694h4v2.694c-1.439-.243-2.592-.238-4 0zm8.851 2.064l1.407-1.407 1.414 1.414-1.321 1.321c-.462-.484-.964-.927-1.5-1.328zm-18.851 4.242h8v2h-8v-2zm-2 4h8v2h-8v-2zm3 4h7v2h-7v-2zm21-3c0 5.523-4.477 10-10 10-2.79 0-5.3-1.155-7.111-3h3.28c1.138.631 2.439 1 3.831 1 4.411 0 8-3.589 8-8s-3.589-8-8-8c-1.392 0-2.693.369-3.831 1h-3.28c1.811-1.845 4.321-3 7.111-3 5.523 0 10 4.477 10 10z"/>                                </svg>
                            <h2 class="text-gray-600 text-xl py-2 w-1/2"><a href="{{ route('students.delays.index', ['student' => $student->id]) }}"> Delays</a> </h2>
                            </div>
                            <div>
                                <success-button text="{{ _('Add delay') }}" link="{{ route('students.delays.create', ['student' => $student->id]) }}"></success-button>
                            </div>
                        </div>
                        <small-table 
                            :items="{{ $student->delays()->get(['id','name','duration','created_at']) }}" 
                            index_link="{{ route('students.delays.index', ['student' => $student->id]) }}"></small-table>
                    </div>
                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection