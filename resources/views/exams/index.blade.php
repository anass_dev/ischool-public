@extends('layouts.app')
@section('content')

<div class="p-2">
    <!-- ===== Breadcrumb ===== -->

    <breadcrumb :items="[

    {'name':'Exams', 'link':'' },

    ]"></breadcrumb>
</div>
 
<div class="container p-2">

        
     
    <div class="flex justify-between py-2 mb-2 rounded items-center">
        <div class="text-2xl text-gray-800 flex">

                <svg class="fill-current text-gray-400 w-10 mx-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                    <path d="M22 0v14.386c0 2.391-6.648 9.614-9.811 9.614h-10.189v-24h20zm-10.638 22c4.156 0 2.638-6 2.638-6s6 1.65 6-2.457v-11.543h-16v20h7.362zm.638-4v1h-5v-1h5zm-5-2h5v1h-5v-1zm0-2h10v1h-10v-1zm0-2h10v1h-10v-1zm3.691-3.174l-2.055.001-.39 1.172-1.246.001 2.113-5.689 1.086-.001 2.133 5.686-1.246.001-.395-1.171zm4.373-2.015l1.41-.001.001 1.019-1.41.001.001 1.594-1.074.001-.001-1.594-1.414.001-.001-1.019 1.414-.001-.001-1.528h1.074l.001 1.527zm-6.112 1.067l1.422-.001-.717-2.129-.705 2.13z"/>
                </svg>
                
                {{_('Exams') }}

            </div>
            <div class="flex justify-start items-center"> 
                
                <secondary-button link="{{ route('home') }}" text="{{ _('Exam Types') }}"></secondary-button>
                <success-button text="{{ _('Add exam') }}" link="{{ route('exams.create') }}"></success-button> 
            </div>
        </div>

        
        <am-table :items="{{ $exams }}" index_link="{{ route('exams.index') }}"></am-table>
        
    </div>


@endsection