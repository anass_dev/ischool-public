<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    /**
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.index',["employees"=>Employee::all(['id','surname','name','email','updated_at','created_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = Employee::create($this->validaterequest());
        return redirect('employees/'.$employee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show')->with(['employee'=>$employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit')->with(['employee'=>$employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Employee $employee)
    {

        $employee->update($this->validateRequest());

        return redirect('employees/'.$employee->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Employee $employee)
    {
        $employee->delete();
    }

    /**
     * 
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([

           'name'               =>'required|min:3|max:20',
           'surname'            =>'required|min:3|max:20',
           'cin'                =>'required',
           'sex'                =>'required|min:1|max:1',
           'phone_number'       =>'required|min:9|max:15',
           'email'              =>'required|email',
           'birthday'           =>'required|date',
           'address'            =>'required',
           'current_salary'     =>'nullable|numeric',
       ]);

    }
}
