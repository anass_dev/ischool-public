@extends('layouts.app')
@section('content')

<div class="p-2">
        <!-- ===== Breadcrumb ===== -->
        
        <breadcrumb :items="[

            {'name':'{{ $student->name.' '.$student->surname }}', 'link':'{{ route('students.show',$student) }}' }, 
            {'name':'debts', 'link':'{{ route('students.debts.index',$student) }}' },
            {'name':'edit', 'link':'' }
        
        ]"></breadcrumb>
    
</div>
  
<div class="container p-2">

    <div class="border shadow-sm rounded-md">

    
        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Add Edit') }}"></form-header>
            <info-person-bar :person="{{ $student }}"></info-person-bar>

        </div>  

        <form action="{{ route('students.debts.update',[$student,$debt]) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                        @method('PUT')
                        @include('students.debts.form')
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('students.debts.index',$student) }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>
        </form>
    
    </div>


</div>





@endsection