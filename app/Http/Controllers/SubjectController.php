<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subject;

class SubjectController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subjects.index')->with(['subjects'=>Subject::all(['id','name','hours_number','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Subject $Subject)
    {
        Subject::create($this->validateRequest());
        return redirect('/subjects');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        return view('subjects.edit')->with( [ 'subject' => $subject ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Subject $subject)
    {
        $subject->update($this->validateRequest());

        return redirect('/subjects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Subject $subject)
    {
        $subject->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'           => 'required|max:100',
            'description'    => 'nullable|max:100',
            'hours_number'   => 'required|numeric',
        ]);

    } 

}
