<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Exam;
use Faker\Generator as Faker;

$factory->define(Exam::class, function (Faker $faker) {
    return [

        'name'          =>  $faker->word(), 
        'exam_type_id'  =>  Factory(App\ExamType::class),
        'subject_id'    =>  Factory(App\Subject::class)
        
    ];
});
