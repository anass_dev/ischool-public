@extends('layouts.app')
@section('content')

<div class="p-2">
    
    <!-- ===== Breadcrumb ===== -->
    
    <breadcrumb :items="[

        {'name':'grades', 'link':'{{ route('grades.index') }}' },
        {'name':'Add', 'link':'' }
    
    ]"></breadcrumb>
            
</div>

  
<div class="container p-2">
    

    <div class="border shadow-sm rounded-md">
            
                    
        <div class="bg-gray-100 rounded-md">
        
  
            <form-header title="{{_('Add Grade') }}"></form-header>
                
        </div>  
                  
        <form action="{{ route('grades.store') }}" method="post">

                    <div class="border-t border-b py-8 px-4">

                            @csrf
                            
                            @include('grades.form')
                        
                    </div>

                    <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                        <back-button link="{{ route('grades.index') }}" text="{{ _('Back') }}"></back-button>
                        <save-button text="Save"></save-button>
                        
                    </div>
            </form>

        </div>
    
        
    </div>

@endsection