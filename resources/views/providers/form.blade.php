<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Name') }}</label>
                    <am-input name="name" type="text" value="{{ $provider->name ?? old('name') }}"  ></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


    <div class="flex flex-wrap">


                <!--=== Adress ===-->

                <div class="w-full md:w-full">

                    <div class="flex flex-col p-4">
                                    
                        <label for="address" class="text-md">{{ _('Address') }}</label>
            
                        <am-input name="address" type="text" value="{{ $provider->address ?? old('address') }}"  ></am-input>
                        
                        @error('address')
            
                            <alert-error message="{{ $message  }}"></alert-error>
            
                        @enderror
            
                    </div>
        
                </div>

    </div>


    <div class="flex flex-wrap">

        
        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="email" class="text-md">{{ _('Email') }}</label>
    
                <am-input name="email" type="email" value="{{ $provider->email ?? old('email') }}"  ></am-input>
                
                @error('email')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
        <!--=== PhoneNumber ===-->

        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="phone_number" class="text-md">{{ _('Phone number') }}</label>
    
                <am-input name="phone_number" type="text" value="{{ $provider->phone_number ?? old('phone_number') }}"  ></am-input>
                
                @error('phone_number')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>



    </div>

  


</div>