<div>


    <div class="flex flex-wrap">


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="mark" class="text-md">{{ _('Mark') }}</label>
                    <am-input name="mark" type="number" value="{{ $result->mark ?? old('mark') }}"></am-input>
                    @error('mark')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="student" class="text-md">{{ _('Student') }}</label>
                    <drop :items="{{ $students }}" name="student" column_name="student_id" selected={{ $result->student_id ?? old('student_id') }}></drop>
                    @error('student_id')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>



</div>