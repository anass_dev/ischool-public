<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Settings;

class Absent extends Model
{

    use SoftDeletes;
    protected $guarded = [];
    
    /**
     * create a many-to-one realtionship with students table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function student(){

        return $this->belongsTo('App\Student');
    }

    public static function message($student) {
        $params = Settings::params();

        if(trim($params->absents->email->message) != "") return trim($params->absents->email->message);

        return "Student " . $student->name . ' ' . $student->surname . ' is absent';  
        
    }
}
