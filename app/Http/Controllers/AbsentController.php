<?php

namespace App\Http\Controllers;

use App\Absent;
use App\Notifications\StudentAbsentNotification;
use App\Events\StudentWasAbsentEvent;

use App\Student;

use Illuminate\Http\Request;

class AbsentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $Student)
    {
        return view('students.absents.index')->with(['absents'=>$Student->absents]);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Student $student)
    {
        return view('students.absents.create')->with(['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $Student)
    {   
        
        $absent  = Absent::make($this->validateRequest());
        $absent->student_id = $Student->id;

        $absent->save();

        event( new StudentWasAbsentEvent($absent) );

        return redirect('/students/'.$Student->id.'/absents');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,Absent $absent)
    {
        return view('students.absents.edit')->with(['student'=>$student,"absent"=>$absent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function update(Student $Student,Absent $absent)
    {
        $absent->update($this->validateRequest());

        return redirect('/students/'.$Student->id.'/absents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Absent  $absent
     * @param  \App\Student  $Student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $Student,Absent $absent)
    {
        $absent->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'          => 'required|max:100',
            'description'   => 'nullable',
            'from'          => 'nullable',
            'to'            => 'nullable',
        ]);

    } 
}
