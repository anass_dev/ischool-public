<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classroom;
use Faker\Generator as Faker;

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        
        'name'      => $faker->word(),
        'capacity'  => $faker->numberBetween(10, 30),
        'occupied'  => $faker->boolean($chanceOfGettingTrue = 70),
        
    ];
});
