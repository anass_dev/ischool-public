<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Complaint;
use Faker\Generator as Faker;

$factory->define(Complaint::class, function (Faker $faker) {
    return [
        
        'name'          =>  $faker->word(),
        'description'   =>  $faker->text(150),
        'resolved'      =>  $faker->boolean($chanceOfGettingTrue = 70),
        'student_id'    =>  factory(App\Student::class)->create()

    ];
});
