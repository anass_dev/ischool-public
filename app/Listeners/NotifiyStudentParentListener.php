<?php

namespace App\Listeners;

use App\Events\StudentWasAbsentEvent;
use App\Mail\StudentAbsentMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
 
class NotifiyStudentParentListener implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  StudentWasAbsentEvent  $event
     * @return void
     */
    public function handle(StudentWasAbsentEvent $event)
    {
       $subject = $event->student->name .' '. $event->student->surname .' was absent';
       Mail::to($event->student)->send(new StudentAbsentMail($event->student, $subject));
    }
}
