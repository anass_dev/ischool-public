<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('first name') }}</label>
                    <am-input name="name" type="text" value="{{ $teacher->name ?? old('name') }}"></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

        <div class="w-full md:w-1/2">
            <div class="flex flex-col p-4">
                                
                <label for="surname" class="text-md">{{ _('last name') }}</label>
                <am-input name="surname" type="text" value="{{ $teacher->surname ?? old('surname') }}"></am-input>
                @error('surname')

                    <alert-error message="{{ $message  }}"></alert-error>

                @enderror

            </div>
        </div>

    </div>




    <div class="flex flex-wrap">

        <!--=== sex ===-->

        <div class="w-full md:w-1/3">

                <div class="flex flex-col p-4 ">
                                    
                    <label for="sex" class="text-md">{{ _('Sex') }}</label>
                    
                    <drop :items="[ {name: 'Male', id: 'm'},  {name: 'Female', id: 'f'}]" column_name="sex" name="sex" selected={{ $teacher->sex ?? old('sex') }}>
                    </drop>

                    
                    @error('sex')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>

        </div>

        <!--=== cin ===-->

        <div class="w-full md:w-1/3">

            <div class="flex flex-col p-4">
                            
                <label for="cin" class="text-md">{{ _('CIN') }}</label>
    
                <am-input name="cin" type="text" value="{{ $teacher->cin ?? old('cin') }}"></am-input>
                
                @error('cin')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
        <!--=== Birthday ===-->
        
        <div class="w-full md:w-1/3">

            <div class="flex flex-col p-4">
                            
                <label for="birthday" class="text-md">{{ _('Birthday') }}</label>
    
                <am-input name="birthday" type="date" value="{{ $teacher->birthday ?? old('birthday') }}"></am-input>
                
                @error('birthday')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>


        
    </div>

    <div class="flex flex-wrap">

        <div class="w-full">


                <div class="flex flex-col p-4">
                                
                    <label for="address" class="text-md">{{ _('Address') }}</label>
        
                    <am-input name="address" type="text" value="{{ $teacher->address ?? old('address') }}"></am-input>
                    
                    @error('address')
        
                        <alert-error message="{{ $message  }}"></alert-error>
        
                    @enderror
        
                </div>

        </div>

    </div>


    <div class="flex flex-wrap">

        
        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="email" class="text-md">{{ _('Email') }}</label>
    
                <am-input name="email" type="email" value="{{ $teacher->email ?? old('email') }}"></am-input>
                
                @error('email')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
        <!--=== PhoneNumber ===-->

        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="phone_number" class="text-md">{{ _('Phone number') }}</label>
    
                <am-input name="phone_number" type="text" value="{{ $teacher->phone_number ?? old('phone_number') }}"></am-input>
                
                @error('phone_number')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>



    </div>




</div>