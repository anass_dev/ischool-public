@extends('layouts.app')
@section('content')


<div class="pt-2 px-4">
    <breadcrumb :items="[

        {'name':'exams', 'link':'{{ route('exams.index') }}' }, 
        {'name':'{{ $exam->name }}', 'link':'' }
    
]"></breadcrumb>
</div>


<div class="container p-2">
    
    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-5 rounded-md shadow-md">

                    
                    <div class="mt-2 flex flex-col items-center justify-center">
                        <div class="text-center">
                            <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                <path d="M22 0v14.386c0 2.391-6.648 9.614-9.811 9.614h-10.189v-24h20zm-10.638 22c4.156 0 2.638-6 2.638-6s6 1.65 6-2.457v-11.543h-16v20h7.362zm.638-4v1h-5v-1h5zm-5-2h5v1h-5v-1zm0-2h10v1h-10v-1zm0-2h10v1h-10v-1zm3.691-3.174l-2.055.001-.39 1.172-1.246.001 2.113-5.689 1.086-.001 2.133 5.686-1.246.001-.395-1.171zm4.373-2.015l1.41-.001.001 1.019-1.41.001.001 1.594-1.074.001-.001-1.594-1.414.001-.001-1.019 1.414-.001-.001-1.528h1.074l.001 1.527zm-6.112 1.067l1.422-.001-.717-2.129-.705 2.13z"/>
                            </svg>
                            
                        </div>

                        <h2 class="p-2 text-xl text-center text-gray-700">{{ $exam->name  }}</h2>
                    </div>

                    <div class="mt-2 flex  items-start">
                        <label class="p-2 text-blue-400"><i class="fas fa-book"></i>  </label>
                        <div class="p-2 text-sm text-gray-700"> {{ $exam->subject->name }}</div>

                    </div>

                    <div class="mt-2 flex  items-start">
                        <label class="p-2 text-blue-400"><i class="fas fa-file-signature"></i></label>
                        <div class="p-2 text-gray-700"> {{ $exam->ExamType->name }} </div>

                    </div>

               
                </div>

            </div>

            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">
                <div class="m-2 p-5 rounded-md shadow-md">
                    <div class="flex items-center justify-between">
                    <h2 class="text-gray-600 text-xl py-5 w-1/2"><a href="{{ route('exams.results.index', ['exam' => $exam->id]) }}"> Results </a></h2>
                        <div>
                            <success-button text="{{ _('Add result') }}" link="{{ route('exams.results.create', ['exam' => $exam->id]) }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $results }}" 
                        index_link="{{ route('exams.results.index', ['exam' => $exam->id]) }}" ></small-table>
       
                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection