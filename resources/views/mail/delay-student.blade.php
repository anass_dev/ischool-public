@component('mail::message')

Hi {{ $notifiable->name }} {{ $notifiable->surname }},

{{ $message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent