<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\SchoolYear;


class SchoolYearControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_school_year(){

        $this->get('/schoolyears')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_school_year(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->get('/schoolyears')->assertStatus(200);

    }




    /** @test */
    public function show_interview_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $this->get('/schoolyears/create')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_school_year(){

       

        $this->actingAs(Factory(User::class)->create());
        $school_year = factory(SchoolYear::class)->create();

        $this->get('/schoolyears/'.$school_year->id)->assertStatus(200);

    }




    /** @test */
    public function a_school_year_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/schoolyears',[
            'name' => 'first',
            'first_day' => '2020-02-01',
            'last_day' => '2020-09-01'
        ]);

        $this->assertCount(1,SchoolYear::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $this->post('/schoolyears',[
                'name' => 'first',
                'first_day' => '2020-02-91',
                'last_day' => '2020-14-01'
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_school_year(){

       

        $this->actingAs(Factory(User::class)->create());

        $school_year = factory(SchoolYear::class)->create();

        $this->get('/schoolyears/'.$school_year->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_school_year_can_be_updated(){

        $this->withoutExceptionHandling();
        $this->actingAs(Factory(User::class)->create());

        $school_year = Factory(SchoolYear::class)->create();
    

        $this->patch('/schoolyears/'.$school_year->id,[
            'name'      => 'first_test',
            'first_day' => '2020-02-01',
            'last_day'  => '2020-09-01'
        ]);

        $this->assertDatabaseHas('school_years', ['name'=>'first_test']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $school_year = Factory(SchoolYear::class)->create();
    
        $this->patch('/schoolyears/'.$school_year->id,[
            
            'name'      => 'first_test',
            'first_day' => '2020-082-01',
            'last_day'  => '20020-09-01'
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('school_years', ['name'=>'first_test']);
    
    
    }




    /** @test */
    public function a_school_year_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $school_year = Factory(SchoolYear::class)->create();
    
        $this->delete('/schoolyears/'.$school_year->id);

        $this->assertSoftDeleted('school_years', ['id'=>$school_year->id]);
        
        
    }
}
