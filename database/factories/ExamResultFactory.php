<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExamResult;
use Faker\Generator as Faker;

$factory->define(ExamResult::class, function (Faker $faker) {
    return [
        'exam_id'       => Factory(App\Exam::class),
        'student_id'    => Factory(App\Student::class),
        'mark'          => $faker->numberBetween(0, 20),
    ];
});
