<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paiment extends Model
{

    use SoftDeletes;
    protected $guarded = [];

    
    /**
    * create a many-to-one realtionship with students table
    * 
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */

   public function student(){

       return $this->belongsTo('App\Student');
   }
}
