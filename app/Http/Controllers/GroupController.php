<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;
use App\Group;
use App\Student;
use App\StudentGroup;

class GroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('groups.index')->with(['groups'=>Group::all(['id','name','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groups.create')->with(['grades'=>Grade::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = Group::create($this->validateRequest());

        return redirect('groups/'.$group->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {   
        $group_students =  $group->students()->get([ 
            'students.id', 
            'students.name', 
            'students.surname', 
            'students.cin', 
            'students.birthday', 
            'student_group.created_at'
         ]);

        return view('groups.show')->with([
            "group"     => $group,
            "students"     => $group_students,

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addStudents(Group $group)
    {   
        return view('groups.students.create')->with(["group"=>$group,"students"=>Student::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeStudents(Request $request,Group $group)
    {   
        $students = json_decode( $request->input('students'));

        
        StudentGroup::where(['group_id'=>$group->id])->delete();
        
        foreach ($students as $student) {

            StudentGroup::firstOrCreate([
                'student_id' => $student->id,
                'group_id' => $group->id,
                
            ]);
        }


        return redirect(route('groups.show',["group"=>$group ]));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('groups.edit')->with(["group"=>$group,'groups'=>Group::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $group->update($this->validateRequest());
        redirect('groups/'.$group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group->delete();
    }

    /**
     *  validates the post request
     *  
     *  @return array
     */
     private function validateRequest(){

        return request()->validate([
            'name'      => 'required|max:100',
            'grade_id'  => 'required|numeric'
        ]);

     }
}
