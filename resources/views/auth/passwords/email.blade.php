@extends('layouts.auth')

@section('content')



<div class="w-full flex flex-wrap">


    <div class="w-1/2 shadow-2xl">
        <img class="object-cover w-full h-screen hidden md:block" 
        src="https://images.unsplash.com/photo-1510070112810-d4e9a46d9e91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    </div>

  <!-- Login Section -->
  <div class="w-full md:w-1/2 flex flex-col">

        <div class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-24">
            <a href="{{ route('home') }}" class="bg-black text-white font-bold text-xl p-4">@include('layouts.main.svg-logo')</a>
        </div>

        <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
            
            <p class="text-center text-3xl">{{ __('Reset Password') }}</p>

            @if (session('status'))

                <alert-success status="{{ session('status') }}" ></alert-success>

            @endif

            <form method="POST"  class="flex flex-col pt-2 md:pt-2" action="{{ route('password.email') }}">
                @csrf


                <div class="flex flex-col pt-4">
                    
                    <label for="password" class="text-lg">{{ __('Email') }}</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Email" class="appearance-none border rounded w-full p-3 text-gray-700 mb-3 leading-tight focus:outline-none @error('email') border-red-500 @enderror" autofocus >
                    
                    @error('email')
                        <alert-error message="{{ $message }}" ></alert-error>
                    @enderror
                
                </div>




                <input type="submit" value="{{ __('Send Password Reset Link') }}" class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8">


            </form>


        </div>



  </div>


</div>



@endsection
