<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complaint extends Model
{

    protected $guarded = [];
    use SoftDeletes;

    
    /**
     * create a many-to-one realtionship with students table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */

    public function student(){

        return $this->belongsTo('App\Student');
    }
}
