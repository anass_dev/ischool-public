<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Name') }}</label>
                    <am-input name="name" type="text" value="{{ $note->name ?? old('name') }}"  ></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="description" class="text-md">{{ _('Description') }}</label>
                    <am-textarea element_name="description" >{{ $note->description ?? old('description') }}</am-textarea>
                    @error('description')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


</div>