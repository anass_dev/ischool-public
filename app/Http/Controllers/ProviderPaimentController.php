<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\ProviderPaiment as Paiment;

class ProviderPaimentController extends Controller
{
    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Provider $provider)
    {
        return view('providers.paiments.index')->with(['paiments'=>$provider->paiments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Provider $provider)
    {
        return view('providers.paiments.create')->with(["provider"=>$provider]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Provider $provider)
    {
        $paiment  = Paiment::make($this->validateRequest());
        $paiment->Provider_id = $provider->id;
        $paiment->save();

        return redirect('/Providers/'.$provider->id.'/paiments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider,Paiment $paiment)
    {
        return view('providers.paiments.edit')->with(["provider"=>$provider,"paiment"=>$paiment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Provider $provider,Paiment $paiment)
    {
        $paiment->update($this->validateRequest());

        return redirect('/Providers/'.$provider->id.'/paiments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider,Paiment $paiment)
    {
        $paiment->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'      => 'required|max:100',
            'method'    => 'required|max:100',
            'amount'    => 'required|numeric',
        ]);
    } 
}
