@extends('layouts.app')
@section('content')

<div class="p-2">
   <!-- ===== Breadcrumb ===== -->
            
    <breadcrumb :items="[

        {'name':'providers', 'link':'{{ route('providers.index') }}' }, 
        {'name':'{{ $provider->name }}', 'link':'' }

    ]"></breadcrumb>
</div>

<div class="container">

    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-5 rounded-md shadow-md">

                
                <div class="mt-2 flex flex-col items-center justify-center">
                    <div class="text-center">
                        <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26">
                            <path d="M7.919 17.377l-4.869-13.377h-2.05c-.266 0-.52-.105-.707-.293-.188-.187-.293-.442-.293-.707 0-.552.447-1 1-1h3.45l5.469 15.025c.841.101 1.59.5 2.139 1.088l11.258-4.097.684 1.879-11.049 4.021c.032.19.049.385.049.584 0 1.932-1.569 3.5-3.5 3.5-1.932 0-3.5-1.568-3.5-3.5 0-1.363.781-2.545 1.919-3.123zm1.581 1.811c.724 0 1.312.588 1.312 1.312 0 .724-.588 1.313-1.312 1.313-.725 0-1.313-.589-1.313-1.313s.588-1.312 1.313-1.312zm5.799-12.29l4.767-1.735 2.736 7.517-11.406 4.152-2.736-7.518 4.759-1.732 1.325 3.639 1.879-.684-1.324-3.639zm.537-1.26l-7.518 2.736-2.052-5.638 7.518-2.736 2.052 5.638z"/>                    </div>

                    <h2 class="p-2 text-xl text-center text-gray-700">{{ $provider->name }}</h2>
                </div>


                <div class="mt-2 flex  items-start">
                    <label class="p-2 pl-0 text-blue-400"><i class="fas fa-phone"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $provider->phone_number }}</div>

                </div>

                <div class="mt-2 flex items-start">
                    <label class="p-2 pl-0 text-blue-400"><i class="fas fa-at"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $provider->email }} </div>

                </div>

                <div class="flex items-start">
                    <label class="p-2 pl-0 text-blue-400"><i class="fas fa-map"></i>  </label>
                    <textarea class="p-2 text-sm text-gray-700" cols="30" rows="3" readonly>{{ $provider->address }}</textarea>
                </div>
            </div>

            </div>

            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">
                <div class="m-2 p-5 rounded-md shadow-md">
                    <div class="flex items-center justify-between">
                    <div class="flex items-center justify-start">
                        <svg class="fill-current text-gray-400 w-8 mx-2 mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                            <path d="M12.164 7.165c-1.15.191-1.702 1.233-1.231 2.328.498 1.155 1.921 1.895 3.094 1.603 1.039-.257 1.519-1.252 1.069-2.295-.471-1.095-1.784-1.827-2.932-1.636zm1.484 2.998l.104.229-.219.045-.097-.219c-.226.041-.482.035-.719-.027l-.065-.387c.195.03.438.058.623.02l.125-.041c.221-.109.152-.387-.176-.453-.245-.054-.893-.014-1.135-.552-.136-.304-.035-.621.356-.766l-.108-.239.217-.045.104.229c.159-.026.345-.036.563-.017l.087.383c-.17-.021-.353-.041-.512-.008l-.06.016c-.309.082-.21.375.064.446.453.105.994.139 1.208.612.173.385-.028.648-.36.774zm10.312 1.057l-3.766-8.22c-6.178 4.004-13.007-.318-17.951 4.454l3.765 8.22c5.298-4.492 12.519-.238 17.952-4.454zm-2.803-1.852c-.375.521-.653 1.117-.819 1.741-3.593 1.094-7.891-.201-12.018 1.241-.667-.354-1.503-.576-2.189-.556l-1.135-2.487c.432-.525.772-1.325.918-2.094 3.399-1.226 7.652.155 12.198-1.401.521.346 1.13.597 1.73.721l1.315 2.835zm2.843 5.642c-6.857 3.941-12.399-1.424-19.5 5.99l-4.5-9.97 1.402-1.463 3.807 8.406-.002.007c7.445-5.595 11.195-1.176 18.109-4.563.294.648.565 1.332.684 1.593z"/>
                        </svg>
                        <h2 class="text-gray-600 text-xl py-5 w-1/2"><a href="{{ route('providers.paiments.index', ['provider' => $provider->id]) }}"> Payments </a></h2>
                        
                    </div>
                        <div>
                            <success-button text="{{ _('Add paiment') }}" link="{{ route('providers.paiments.create', ['provider' => $provider->id]) }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $provider->paiments()->get(['id','amount','created_at']) }}" 
                        index_link="{{ route('providers.paiments.index', ['provider' => $provider->id]) }}" ></small-table>

                </div>

                <div class="m-2 p-5 rounded-md shadow-md">
                    <div class="flex items-center justify-between">
                        
                        <div class="flex items-center justify-start">
                            <svg class="fill-current text-gray-400 w-8 mx-2 mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                <path d="M21.5 6c.276 0 .5.224.5.5v11c0 .276-.224.5-.5.5h-19c-.276 0-.5-.224-.5-.5v-11c0-.276.224-.5.5-.5h19zm2.5 0c0-1.104-.896-2-2-2h-20c-1.104 0-2 .896-2 2v12c0 1.104.896 2 2 2h20c1.104 0 2-.896 2-2v-12zm-20 3.78c0-.431.349-.78.78-.78h.427v1.125h-1.207v-.345zm0 .764h1.208v.968h-1.208v-.968zm0 1.388h1.208v1.068h-.428c-.431 0-.78-.349-.78-.78v-.288zm4 .288c0 .431-.349.78-.78.78h-.429v-1.068h1.209v.288zm0-.708h-1.209v-.968h1.209v.968zm0-1.387h-1.629v2.875h-.744v-4h1.593c.431 0 .78.349.78.78v.345zm5.5 2.875c-1.381 0-2.5-1.119-2.5-2.5s1.119-2.5 2.5-2.5c.484 0 .937.138 1.32.377-.53.552-.856 1.3-.856 2.123 0 .824.326 1.571.856 2.123-.383.239-.836.377-1.32.377zm1.5-2.5c0-1.381 1.12-2.5 2.5-2.5 1.381 0 2.5 1.119 2.5 2.5s-1.119 2.5-2.5 2.5c-1.38 0-2.5-1.119-2.5-2.5zm-8 4.5h-3v1h3v-1zm4 0h-3v1h3v-1zm5 0h-3v1h3v-1zm4 0h-3v1h3v-1z"/>                            </svg>
                            <h2 class="text-gray-600 text-xl py-5 w-1/2">
                                <a href="{{ route('providers.debts.index', ['provider' => $provider->id]) }}"> Debts </a>
                            </h2>
                            
                        </div>

                        
                        <div>
                            <success-button text="{{ _('Add debts') }}" link="{{ route('providers.debts.create', ['provider' => $provider->id]) }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $provider->debts()->get(['id','amount','created_at']) }}" 
                        index_link="{{ route('providers.debts.index', ['provider' => $provider->id]) }}" ></small-table>

                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection