<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Debt;
use App\Student;


class DebtControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_debt(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/debts')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_debt(){


        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/debts')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_debt(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/debts/create')->assertStatus(200);

    }




    /** @test */
    public function a_debt_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/debts',[
            'name'=> 'debt_name',
            'amount' => 500,
        ]);

        $this->assertCount(1,Debt::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/debts',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_debt(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $debt = factory(Debt::class)->create();


        $this->get('/students/'.$debt->student_id.'/debts/'.$debt->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_debt_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $debt = Factory(Debt::class)->create();
    

        $this->patch('/students/'.$debt->student_id.'/debts/'.$debt->id,[
            'name'      => 'test_update',
            'amount'  => 5
        ]);

        $this->assertDatabaseHas('debts', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $debt     = Factory(Debt::class)->create();
    
        $this->patch('/students/'.$debt->student_id.'/debts/'.$debt->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('debts', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_debt_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $debt = Factory(Debt::class)->create();
    
        $this->delete('/students/'.$debt->student_id.'/debts/'.$debt->id);
        

        $this->assertSoftDeleted('debts', ['id'=>$debt->id]);
        
    }
}
