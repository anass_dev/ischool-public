<?php

namespace App\Http\Controllers;

use App\Student;
use App\Group;
use App\StudentGroup;
use Illuminate\Http\Request;

class GroupStudentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Group $group)
    {
        //$group = StudentGroup::create($this->validateRequest());

        return redirect('groups/'.$group->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('groups.students.edit')->with(["group"=>$group,'groups'=>Group::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $group->update($this->validateRequest());
        redirect('groups/'.$group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group,Student $student)
    {
        $group->delete();
    }

    /**
     *  validates the post request
     *  
     *  @return array
     */
     private function validateRequest(){

        return request()->validate([
            'name'      => 'required|max:100',
            'grade_id'  => 'required|numeric'
        ]);

     }
}
