<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Student;
use App\User;
use App\Paiment;

class PaimentControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_paiment(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/paiments')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_paiment(){


        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/paiments')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_paiment(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/paiments/create')->assertStatus(200);

    }




    /** @test */
    public function a_paiment_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/paiments',[
            'name'=> 'paiment_name',
            'method'=> 'cash',
            'amount' => 500,
        ]);

        $this->assertCount(1,Paiment::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/paiments',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_paiment(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $paiment = factory(Paiment::class)->create();


        $this->get('/students/'.$paiment->student_id.'/paiments/'.$paiment->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_paiment_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $paiment = Factory(Paiment::class)->create();
    

        $this->patch('/students/'.$paiment->student_id.'/paiments/'.$paiment->id,[
            'name'      => 'test_update',
            'method'=> 'cart',
            'amount'  => 5
        ]);

        $this->assertDatabaseHas('paiments', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $paiment     = Factory(Paiment::class)->create();
    
        $this->patch('/students/'.$paiment->student_id.'/paiments/'.$paiment->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('paiments', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_paiment_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $paiment = Factory(Paiment::class)->create();
    
        $this->delete('/students/'.$paiment->student_id.'/paiments/'.$paiment->id);
        

        $this->assertSoftDeleted('paiments', ['id'=>$paiment->id]);
        
    }
}
