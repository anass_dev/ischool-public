<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    
    use SoftDeletes;
    protected $guarded = [];

    /**
     * 
     * 
     * 
     */

     public function results(){
         return $this->hasMany('App\ExamResult');
     }

    /**
     * 
     * 
     * 
     */

    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    /**
     * 
     * 
     * 
     */

    public function ExamType(){
        return $this->belongsTo('App\ExamType');
    }



}
