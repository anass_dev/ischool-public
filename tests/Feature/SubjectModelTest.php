<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App;

class SubjectTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 
     * tests if the subject correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_subject_is_saved(){

        
        $subject = factory(App\Subject::class)->create();

        $subject->save();

        $this->assertDatabaseHas('subjects', ['id'=>$subject->id]);


    }   




    /**
     * 
     * tests if the subject correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_subject_is_updated(){
        
        $subject = factory(App\Subject::class)->create();
        
        $subject->update(['name' => 'sepcial_name_test']);
        
        $this->assertDatabaseHas('subjects', ['name' =>'sepcial_name_test']);

    }




    /**
     * 
     * tests if the subject correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_subject_is_softDeleted(){

        $subject = factory(App\Subject::class)->create();

        $subject->delete();

        $this->assertSoftDeleted($subject);

    }




    /**
     * 
     * tests if the subject correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_subject_founde(){

        $subject = factory(App\Subject::class)->create();
        $subject->save();


        $new_parent = App\Subject::find($subject->id);

        $this->assertEquals($subject->id,$new_parent->id);

    }

 


    /**
     * 
     * tests if the subject exams relationship 
     * 
     * @return void
     * 
     */

     
    public function test_subject_exams_relationship(){

        $exam = factory(App\Exam::class)->create();
        $subject = App\Subject::find($exam->subject_id);

        $this->assertGreaterThan(0,$subject->exams->count());

    }




}
