<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Employee;
use App\EmployeeSalary as Salary;


class EmployeeSalaryControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function non_login_user_can_not_see_salaries(){

        $employee = factory(Employee::class)->create();
        $this->get('/employees/'.$employee->id.'/salaries')->assertRedirect('login');

    }




    /** @test */
    public function login_user_can_see_salaries(){

        $this->actingAs(factory(User::class)->create());

        $employee = factory(Employee::class)->create();
        $this->get('/employees/'.$employee->id.'/salaries')->assertStatus(200);

    }



    /** @test */
    public function create_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());
        $employee = factory(Employee::class)->create();

        $this->get('/employees/'.$employee->id.'/salaries/create')->assertStatus(200);

    }




    /** @test */
    public function single_result_show_view_is_not_accssible(){


        $this->actingAs(Factory(User::class)->create());

        $salary = factory(Salary::class)->create();
        $employee   = factory(Employee::class)->create();

        $this->get('/employees/'.$employee->id.'/salaries/'.$salary->id)->assertStatus(405);

    }



    /** @test */
    public function employee_salary_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());


        $employee       = factory(Employee::class)->create();

        $this->post('/employees/'.$employee->id.'/salaries',[

                'amount'          => 8000,
                
        ])->assertRedirect('/employees/'.$employee->id.'/salaries');

        $this->assertCount(1,Salary::all());

    }





    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());


        $employee       = factory(Employee::class)->create();


        $this->post('/employees/'.$employee->id.'/salaries',[
            'amount'          => false,

        ])->assertSessionHasErrors();


    }




    /** @test */
    public function view_update_is_accssible_employeesalary(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());



        $salary = factory(Salary::class)->create();


        $this->get('/employees/'.$salary->employee_id.'/salaries/'.$salary->id.'/edit')->assertStatus(200);


    }




    /** @test */
    public function result_is_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $salary = Factory(Salary::class)->create();

        $this->patch('/employees/'.$salary->employee_id.'/salaries/'.$salary->id,[
            
            'amount'          => 8940,
            
        ]);

        $this->assertDatabaseHas('employee_salaries', ['amount'=>8940]);


    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $salary = Factory(Salary::class)->create();

        $this->patch('/employees/'.$salary->employee_id.'/salaries/'.$salary->id,[
            
            'mark'=> '1ds',


        ])->assertSessionHasErrors();

    }


        /** @test */
    public function a_employeesalary_can_be_deleted(){

        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());


        $salary = Factory(Salary::class)->create();


        $this->delete('/employees/'.$salary->employee_id.'/salaries/'.$salary->id);


        $this->assertSoftDeleted('employee_salaries', ['id'=>$salary->id]);
        
        
    }
}
