<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->id();

            $table->string('name',20);
            $table->string('surname',20);

            $table->string('cin',15)->unique();
            $table->string('sex',1);
            
            $table->string('phone_number',15)->unique();
            $table->string('email',40)->unique();

            $table->date('birthday');
            $table->text('address');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
