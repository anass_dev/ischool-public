@extends('layouts.app')
@section('content')

    <div class="flex flex-wrap mt-3">
    
        <div class="w-full md:w-1/3">

           <teachers-card></teachers-card>
    
        </div>

        <div class="w-full md:w-1/3">

            <classrooms-card></classrooms-card>

        </div>

        <div class="w-full md:w-1/3">
            <subjects-card></subjects-card>
        </div>

        
        
    </div>

@endsection