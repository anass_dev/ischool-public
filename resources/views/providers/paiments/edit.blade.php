@extends('layouts.app')
@section('content')

<div class="p-2">
       <!-- ===== Breadcrumb ===== -->
        
       <breadcrumb :items="[

        {'name':'{{ $provider->name.' '.$provider->surname }}', 'link':'{{ route('providers.show',$provider) }}' }, 
        {'name':'paiments', 'link':'{{ route('providers.paiments.index',$provider) }}' },
        {'name':'edit', 'link':'' }
    
    ]"></breadcrumb>

</div>
  
<div class="container p-2">
    
 
    

    <div class="border shadow-sm rounded-md">

    
        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Edit Paiment') }}"></form-header>
            <info-person-bar :person="{{ $provider }}"></info-person-bar>

        </div>  

        <form action="{{ route('students.debts.update',[$provider,$paiment]) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                        @method('PUT')
                        @include('providers.paiments.form')
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('providers.paiments.index',$provider) }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>
        </form>
    
    </div>


</div>





@endsection