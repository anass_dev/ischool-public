<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{

    use SoftDeletes;
    
    protected $guarded = [];
    
    /**
     * The roles that belong to the user.
     */
    public function Students()
    {
        return $this->belongsToMany('App\Student', 'student_group')->whereNull('student_group.deleted_at')->withTimestamps();
    }
    
    /**
     * The roles that belong to the user.
     */
    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

}
