<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\Pivot;

class StudentGroup extends Pivot
{
    
    use SoftDeletes;

    protected $guarded = [];
    protected $table = "student_group";

}
