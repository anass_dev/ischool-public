<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Debt;
use App\Student;

class DebtController extends Controller
{ 
    
    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Student $student)
    {
        return view('students.debts.index',["debts"=>$student->debts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Student $student)
    {
        return view('students.debts.create',['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request,Student $student)
    {
        $debt  = Debt::make($this->validateRequest());
        $debt->student_id = $student->id;
        $debt->save();

        return redirect('/students/'.$student->id.'/debts');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Student $student,Debt $debt)
    {
        return view('students.debts.edit')->with(['debt'=>$debt,"student"=>$student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Student $student,Debt $debt)
    {
        $debt->update($this->validateRequest());

        return redirect('/students/'.$student->id.'/debts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Student $student,Debt $debt)
    {
        $debt->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([

            'name'          => 'required|max:100',
            'amount'        => 'required|numeric',
            
        ]);


    } 
}
