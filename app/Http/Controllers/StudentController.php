<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Student;


class StudentController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.index')->with(["students"=>Student::all(['id','surname','name','email','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create')->with(['grades'=>App\Grade::all(),'parents'=>App\StudentParent::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Student $student)
    {
        $student = Student::create($this->validateRequest());

        return redirect('student/'.$student->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students.show')->with(['student'=>$student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.edit')->with([

            'student'=>$student,
            'grades'=>App\Grade::all(),
            'parents'=>App\StudentParent::all()
            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Student $student)
    {
        $student->update($this->validateRequest());
        return redirect('students/'.$student->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
    }

    /**
     * 
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([

           'name'          =>'required|min:3|max:20',
           'surname'       =>'required|min:3|max:20',
           'cin'           =>'required',
           'sex'           =>'required|min:1|max:1',
           'phone_number'  =>'required|min:9|max:15',
           'email'         =>'required|email',
           'birthday'      =>'required|date',
           'address'       =>'required',
           'parent_id'     =>'required',
           'grade_id'      =>'required',
       ]);

    }
}
