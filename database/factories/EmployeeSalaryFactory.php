<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmployeeSalary;
use Faker\Generator as Faker;

use Illuminate\Support\Str;


$factory->define(EmployeeSalary::class, function (Faker $faker) {
    return [
        'amount'        =>  $faker->randomFloat(2,2500,5500),
        'employee_id'   =>  factory(App\Employee::class),
    ];
});
