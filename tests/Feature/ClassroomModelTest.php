<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App;


class ClassroomTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 
     * tests if the subject correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_classroom_is_saved(){

        
        $Classroom = factory(App\Classroom::class)->create();

        $Classroom->save();

        $this->assertDatabaseHas('classrooms', ['id'=>$Classroom->id]);


    }   




    /**
     * 
     * tests if the subject correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_classroom_is_updated(){
        
        $Classroom = factory(App\Classroom::class)->create();
        
        $Classroom->update(['name' => 'sepcial_name_test']);
        
        $this->assertDatabaseHas('classrooms', ['name' =>'sepcial_name_test']);

    }




    /**
     * 
     * tests if the subject correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_classroom_is_softDeleted(){

        $Classroom = factory(App\Classroom::class)->create();

        $Classroom->delete();

        $this->assertSoftDeleted($Classroom);

    }




    /**
     * 
     * tests if the subject correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_classroom_founde(){

        $classroom = factory(App\Classroom::class)->create();
        $classroom->save();


        $new_classroom = App\Classroom::find($classroom->id);

        $this->assertEquals($classroom->id,$new_classroom->id);

    }

}
