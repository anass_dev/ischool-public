<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use App;
use Tests\TestCase;


class ProviederModelTest extends TestCase
{

    use RefreshDatabase;


    
    /**
     * 
     * tests if the provider correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_provider_is_saved(){

        
        $provider = factory(App\Provider::class)->create();


        $provider->save();

        $this->assertDatabaseHas('providers', ['id'=>$provider->id]);


    }
    




    /**
     * 
     * tests if the provider correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_provider_is_updated(){
        
        $provider = factory(App\Provider::class)->create();
        
        $provider->update(['email' => 'test@testemail.com']);
        
        $this->assertDatabaseHas('providers', ['email' =>'test@testemail.com']);

    }




    /**
     * 
     * tests if the provider correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_provider_is_softDeleted(){

        $provider = factory(App\Provider::class)->create();

        $provider->delete();

        $this->assertSoftDeleted($provider);

    }




    /**
     * 
     * tests if the provider correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_provider_founde(){

        $provider = factory(App\Provider::class)->create();
        $provider->save();


        $new_parent = App\Provider::find($provider->id);

        $this->assertEquals($provider->id,$new_parent->id);

    }




    /**
     * 
     * tests the provider paiments relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_provider_paimrnts_relationship(){

        $provider = factory(App\Provider::class)->create();

        $paiment = factory(App\ProviderPaiment::class)->create();

        $paiment->provider_id = $provider->id;

        $paiment->save();


        $this->assertGreaterThan(0,$provider->paiments->count());


    }
    




    /**
     * 
     * tests the provider debts relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_provider_debts_relationship(){

        $provider = factory(App\Provider::class)->create();

        $debt = factory(App\ProviderDebt::class)->create();

        $debt->provider_id = $provider->id;

        $debt->save();


        $this->assertGreaterThan(0,$provider->debts->count());


    }





}

