@extends('layouts.app')
@section('content')


<div class="p-2">
    <breadcrumb :items="[

        {'name':'groups', 'link':'{{ route('groups.index') }}' }, 
        {'name':'{{ $group->name }}', 'link':'{{ route('groups.show',['group'=>$group]) }}' },
        {'name':'students', 'link':'' },
    
]"></breadcrumb>
</div>

<div class="container p-2">
<div class="border shadow-sm rounded-md">

    
    <div class="bg-gray-100 rounded-md">

        <form-header title="{{_('Add Students') }}"></form-header>

    </div>  

        <form action="{{ route('groups.students.store',['group'=>$group]) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                
                <multi-drop :items="{{ $students }}" name="students" column_name="students" :selects="{{ $group->students }}"></multi-drop>
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('groups.show',['group'=>$group]) }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>

        </form>

    </div>

    
</div>
</div>
@endsection