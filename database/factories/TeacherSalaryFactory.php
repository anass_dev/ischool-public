<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TeacherSalary;
use Faker\Generator as Faker;




$factory->define(TeacherSalary::class, function (Faker $faker) {
    return [
        'amount'        =>  $faker->randomFloat(2,2500,5500),
        'teacher_id'   =>  factory(App\Teacher::class)
    ];
});
