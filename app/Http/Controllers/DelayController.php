<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Delay;
use App\Student;

use App\Events\StudentWasDelayedEvent;

class DelayController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $student)
    {
        return view('students.delays.index')->with(['delays'=>$student->delays]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $student)
    {
        return view('students.delays.create')->with(['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $student)
    {
        $delay  = Delay::make($this->validateRequest());
        
        $delay->student_id = $student->id;
        $delay->save();


        event( new StudentWasDelayedEvent($delay) );


        return redirect('/students/'.$student->id.'/delays');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,Delay $delay)
    {
        return view('students.delays.edit')->with(['delay'=>$delay,"student"=>$student]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Student $student,Delay $delay)
    {
        $delay->update($this->validateRequest());

        return redirect('/students/'.$student->id.'/delays');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Student $student,Delay $delay)
    {
        $delay->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'          => 'required|max:100',
            'description'   => 'nullable',
            'duration'      => 'nullable|numeric',
        ]);

    } 

}
