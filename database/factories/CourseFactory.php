<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;
use Carbon\Carbon; 

$factory->define(Course::class, function (Faker $faker) {
    
    $start_hour =  $faker->numberBetween(8, 20);
    $start_minut = $faker->numberBetween(1, 60);

    return [
        
        'group_id' =>Factory(App\Group::class),
        'classroom_id' =>Factory(App\Classroom::class),

        'subject_id' => Factory(App\Subject::class),

        'teacher_id' => Factory(App\Teacher::class),

        'standard' =>$faker->boolean($chanceOfGettingTrue = 70),

        'day' => $faker->numberBetween(1, 7),
        'from' => $start_hour.':'.$faker->numberBetween(1, 60),
        'to' => ($start_hour+1).':'.$start_minut
    ];
});
