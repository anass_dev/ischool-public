<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Debt;
use Faker\Generator as Faker;

$factory->define(Debt::class, function (Faker $faker) {
    return [

        'name'          =>  $faker->word(),
        'amount'        =>  $faker->randomFloat(2,2500,10500),
        'resolved'      =>  $faker->boolean($chanceOfGettingTrue = 70),
        'student_id'   =>  factory(App\Student::class)

    ];
});
