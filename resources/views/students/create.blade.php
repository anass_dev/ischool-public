@extends('layouts.app')
@section('content')


    <div class="p-2">
        <breadcrumb :items="[

            {'name':'students', 'link':'{{ route('students.index') }}' }, 
            {'name':'add', 'link':'' }

        ]"></breadcrumb>
    </div> 

    <div class="container p-2">
    

            <div class="border shadow-sm rounded-md">
        
                
                        <div class="bg-gray-100 rounded-md">
        
                            <!-- ===== Breadcrumb ===== -->
        
                
                            <form-header title="{{_('Add Student') }}"></form-header>
                
                        </div>  
                    
                         
                    

            <form action="{{ route('students.store') }}" method="post">

                <div class="border-t border-b py-8 px-4">
                        @csrf
                        @include('students.form',[$grades,$parents])
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('students.index') }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>

            </form>

        </div>
    
        
    </div>

@endsection