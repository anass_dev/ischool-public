<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Note;
use App\Student;

class NoteControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_note(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/notes')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_note(){


        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/notes')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_note(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/notes/create')->assertStatus(200);

    }




    /** @test */
    public function a_note_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/notes',[
            'name'=> 'note_name',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        ]);

        $this->assertCount(1,Note::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/notes',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_note(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $note = factory(Note::class)->create();


        $this->get('/students/'.$note->student_id.'/notes/'.$note->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_note_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $note = Factory(Note::class)->create();
    

        $this->patch('/students/'.$note->student_id.'/notes/'.$note->id,[
            'name'      => 'test_update',
            'duration'  => 5
        ]);

        $this->assertDatabaseHas('notes', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $note     = Factory(Note::class)->create();
    
        $this->patch('/students/'.$note->student_id.'/notes/'.$note->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('notes', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_note_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $note = Factory(Note::class)->create();
    
        $this->delete('/students/'.$note->student_id.'/notes/'.$note->id);
        

        $this->assertSoftDeleted('notes', ['id'=>$note->id]);
        
    }
}
