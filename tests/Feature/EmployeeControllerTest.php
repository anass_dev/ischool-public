<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Employee;


class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_employee(){

        $response = $this->get('/employees')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_employee(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/employees')->assertStatus(200);

    }




    /** @test */
    public function show_interview_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/employees/create')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_employee(){

       

        $this->actingAs(Factory(User::class)->create());
        $employee = factory(Employee::class)->create();

        $response = $this->get('/employees/'.$employee->id)->assertStatus(200);

    }




    /** @test */
    public function a_employee_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $response = $this->post('/employees',[
            'name'=> 'Parentname',
            'surname'=>'Parentsurname',

            'cin'=> 'R346885',
            'sex'=>'f',
            
            'phone_number'=>'0661648497',
            'email'=>'email@email.com',

            'birthday'=>'1990-01-01',
            'address'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 

        ]);

        $this->assertCount(1,Employee::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $response = $this->post('/employees',[
                'name'=> 'me',
                'surname'=>'',
    
                'cin'=> '44',
                'sex'=>'',
                
                'phone_number'=>'0667',
                'email'=>'email@emailcom',
    
                'birthday'=>'19f0-01-01',
                'address'=>'' 
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_employee(){

       

        $this->actingAs(Factory(User::class)->create());

        $employee = factory(Employee::class)->create();

        $response = $this->get('/employees/'.$employee->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_employee_can_be_updated(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $employee = Factory(Employee::class)->create();
    
        $response = $this->patch('/employees/'.$employee->id,[
                'name'=> 'Parentname',
                'surname'=>'Parentsurname',
    
                'cin'=> 'R346885',
                'sex'=>'f',
                            
                'phone_number'=>'0661648497',
                'email'=>'email@email.com',
    
                'birthday'=>'1990-01-01',
                'address'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 
        ]);

        $this->assertDatabaseHas('employees', ['surname'=>'Parentsurname']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $employee = Factory(Employee::class)->create();
    
        $response = $this->patch('/employees/'.$employee->id,[
                'name'=> 'P',
                'surname'=>'Parname',
    
                'cin'=> 'R85',
                'sex'=>'',
                            
                'phone_number'=>'97',
                'email'=>'email@eom',
    
                'birthday'=>'m0-01-01',
                'address'=>'Lore enim culpa ipsa laborum.' 
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('employees', ['surname'=>'Parname']);
    
    
    }




    /** @test */
    public function a_employee_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $employee = Factory(Employee::class)->create();
    
        $response = $this->delete('/employees/'.$employee->id);

        $this->assertSoftDeleted('employees', ['id'=>$employee->id]);
        
        
    }
}
