@extends('layouts.app')
@section('content')

<div class="p-2">
        
    <breadcrumb :items="[

        {'name':'{{ $student->name.' '.$student->surname }}', 'link':'{{ route('students.show',$student) }}' }, 
        {'name':'notes', 'link':'{{ route('students.notes.index',$student) }}' },
        {'name':'add', 'link':'' }
    
    ]"></breadcrumb>

</div>

<div class="container p-2">
    <!-- ===== Breadcrumb ===== -->
 
    <div class="border shadow-sm rounded-md">


        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Add Note') }}"></form-header>
            <info-person-bar :person="{{ $student }}"></info-person-bar>

        </div>  

        <form action="{{ route('students.notes.store',$student) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                        
                        @include('students.notes.form')
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('students.notes.index',$student) }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>
        </form>

    </div>

</div>





@endsection