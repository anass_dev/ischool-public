<?php

namespace App\Http\Controllers;
use Storage;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    
    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){

        $settings_params =  json_decode( Storage::get('settings.json') );

        return view('settings')->with(['params' => $settings_params]);
    }

    public function update(Request $request){

        $setting_params = [];

        

        $enable_absents_email = $request->input('absents_email')  =="on"    ? true : false ; 
        $enable_absents_sms   = $request->input('absents_sms')    =="on"    ? true : false ; 

        $enable_delays_email  = $request->input('delays_email')  =="on"    ? true : false ; 
        $enable_delays_sms    = $request->input('delays_sms')    =="on"    ? true : false ; 

        $absents_message      = $request->input('absents_message');
        $delays_message       = $request->input('delays_message');


        # absents
        $setting_params['absents']['email']['enabled'] = $enable_absents_email;
        $setting_params['absents']['email']['message'] = $absents_message;

        $setting_params['absents']['sms']['enabled']   = $enable_absents_sms;
        $setting_params['absents']['sms']['message']   = $absents_message;
       
        # delays
        $setting_params['delays']['email']['enabled'] = $enable_delays_email;
        $setting_params['delays']['email']['message'] = $delays_message;

        $setting_params['delays']['sms']['enabled']   = $enable_delays_sms;
        $setting_params['delays']['sms']['message']   = $delays_message;


        Storage::put('settings.json',json_encode($setting_params));

        $request->session()->flash('success', 'Your settings saved successfully. ');
        
        return redirect('settings');


    }
}
