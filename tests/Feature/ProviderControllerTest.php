<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Provider;


class ProviderControllerTest extends TestCase
{
    
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_provider(){

        $response = $this->get('/providers')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_provider(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/providers')->assertOk();

    }




    /** @test */
    public function show_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/providers/create')->assertOk();

    }




    /** @test */
    public function provider_view_create_is_accssible(){

       

        $this->actingAs(Factory(User::class)->create());
        $provider = factory(Provider::class)->create();

        $response = $this->get('/providers/'.$provider->id)->assertOk();

    }




    /** @test */
    public function provider_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        
        $this->post('/providers',[

            'name'          =>  'providername',   
            'phone_number'  => '0666673732',
            'email'         => 'email@email.com',
            'address'       => '04 rue something'
        ]);

        $this->assertCount(1,Provider::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/providers',[
            'name'          =>  '',
            'phone_number'  => '0660',
            'email'         => 'emaiil.com',
            'address'       => '0rue something'
        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_provider(){

       

        $this->actingAs(Factory(User::class)->create());

        $provider = factory(Provider::class)->create();

        $response = $this->get('/providers/'.$provider->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_provider_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $provider = Factory(Provider::class)->create();
    
        $this->patch('/providers/'.$provider->id,[

            'name'          =>  'new_name_provider',   
            'phone_number'  => '0666673732',
            'email'         => 'email@email.com',
            'address'       => '04 rue something'
    
        ]);

        $this->assertDatabaseHas('providers', ['name'=>'new_name_provider']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $provider = Factory(Provider::class)->create();
    
        $this->patch('/providers/'.$provider->id,[

                'name'          => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('providers', ['name'=>'']);

    
    }




    /** @test */
    public function a_provider_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $provider = Factory(Provider::class)->create();

    
        $this->delete('/providers/'.$provider->id);
        $this->assertSoftDeleted('providers', ['id'=>$provider->id]);
        
        
    }
}
