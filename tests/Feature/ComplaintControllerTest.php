<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\User;
use App\Complaint;
use App\Student;


class ComplaintControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_complaint(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/complaints')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_complaint(){


        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/complaints')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_complaint(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/complaints/create')->assertStatus(200);

    }




    /** @test */
    public function a_complaint_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/complaints',[
            'name'=> 'complaint_name',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        ]);

        $this->assertCount(1,Complaint::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/complaints',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_complaint(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $complaint = factory(Complaint::class)->create();


        $this->get('/students/'.$complaint->student_id.'/complaints/'.$complaint->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_complaint_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $complaint = Factory(Complaint::class)->create();
    

        $this->patch('/students/'.$complaint->student_id.'/complaints/'.$complaint->id,[
            'name'      => 'test_update',
            'duration'  => 5
        ]);

        $this->assertDatabaseHas('complaints', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $complaint     = Factory(Complaint::class)->create();
    
        $this->patch('/students/'.$complaint->student_id.'/complaints/'.$complaint->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('complaints', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_complaint_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $complaint = Factory(Complaint::class)->create();
    
        $this->delete('/students/'.$complaint->student_id.'/complaints/'.$complaint->id);
        

        $this->assertSoftDeleted('complaints', ['id'=>$complaint->id]);
        
    }
}
