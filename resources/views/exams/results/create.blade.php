@extends('layouts.app')
@section('content')

  
<div class="container p-2">
    
    <!-- ===== Breadcrumb ===== -->
        
    <breadcrumb :items="[

        {'name':'{{ $exam->name }}', 'link':'{{ route('exams.results.index',$exam) }}' }, 
        {'name':'results', 'link':'{{ route('exams.results.index',$exam) }}' },
        {'name':'add', 'link':'' }
    
    ]"></breadcrumb>

    

    <div class="border shadow-sm mt-4 rounded-md">

    
        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Add Result') }}"></form-header>
            <info-person-bar :person="{{ $exam }}"></info-person-bar>

        </div>  

        <form action="{{ route('exams.results.store',$exam) }}" method="post">

                <div class="border-t border-b py-8 px-4">

                        @csrf
                        
                        @include('exams.results.form')
                    
                </div>

                <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                    <back-button link="{{ route('exams.results.index',$exam) }}" text="{{ _('Back') }}"></back-button>
                    <save-button text="Save"></save-button>
                    
                </div>
        </form>
    
    </div>


</div>





@endsection