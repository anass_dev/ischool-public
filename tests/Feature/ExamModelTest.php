<?php

namespace Tests\Feature;

use App;
use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExamTest extends TestCase
{

    use RefreshDatabase;

    /**
     * 
     * tests if the exam correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_exam_is_saved(){

        
        $exam = factory(App\Exam::class)->create();

        $exam->save();

        $this->assertDatabaseHas('exams', ['id'=>$exam->id]);


    }   




    /**
     * 
     * tests if the exam correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_exam_is_updated(){
        
        $exam = factory(App\Exam::class)->create();
        
        $exam->update(['name' => 'sepcial_name_test']);
        
        $this->assertDatabaseHas('exams', ['name' =>'sepcial_name_test']);

    }




    /**
     * 
     * tests if the exam correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_exam_is_softDeleted(){

        $exam = factory(App\Exam::class)->create();

        $exam->delete();

        $this->assertSoftDeleted($exam);

    }




    /**
     * 
     * tests if the exam correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_exam_founde(){

        $exam = factory(App\Exam::class)->create();
        $exam->save();


        $new_parent = App\Exam::find($exam->id);

        $this->assertEquals($exam->id,$new_parent->id);

    }

    


    /**
     * 
     * tests if the exam correctly founded from  database
     * 
     * @return void
     * 
     */

    public function test_exam_results_relationship(){

        
        $results = factory(App\ExamResult::class)->create();
        $exam = App\Exam::find($results->exam_id);


        $this->assertGreaterThan(0,$exam->results->count());

    }

}
