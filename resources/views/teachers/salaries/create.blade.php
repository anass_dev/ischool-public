@extends('layouts.app')
@section('content')


    <div class="container p-2">

        <!-- ===== Breadcrumb ===== -->
        
        <breadcrumb :items="[

            {'name':'{{ $teacher->name.' '.$teacher->surname }}', 'link':'{{ route('teachers.show',$teacher) }}' }, 
            {'name':'salaries', 'link':'{{ route('teachers.salaries.index',$teacher) }}' },
            {'name':'add', 'link':'' }
        
        ]"></breadcrumb>



        

        <div class="mt-4 border shadow-sm rounded-md">


            <div class="bg-gray-100 rounded-md">
                

                <div class="block py-6 px-6 mx-1 text-2xl text-gray-700"> {{_('Add Salary') }}  </div>

                <info-person-bar :person="{{ $teacher }}"></info-person-bar>
 
                    
            </div>  
            

            <form action="{{ route('teachers.salaries.store',$teacher) }}" method="post">

                    <div class="border-t border-b py-8 px-4">

                            @csrf
                            
                            @include('teachers.salaries.form')
                        
                    </div>

                    <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                        <back-button link="{{ route('teachers.salaries.index',$teacher) }}" text="{{ _('Back') }}"></back-button>
                        <save-button text="Save"></save-button>
                        
                    </div>
            </form>

        </div>
    
        
    </div>

@endsection