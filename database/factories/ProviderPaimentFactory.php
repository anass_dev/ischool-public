<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProviderPaiment;
use Faker\Generator as Faker;


$factory->define(ProviderPaiment::class, function (Faker $faker) {
    return [

        'name'          =>  $faker->word(),

        'method'        =>  $faker->randomElement(['cash', 'chique']),
        'amount'        =>  $faker->randomFloat(2,2500,10500),
        
        'status'        =>  $faker->randomElement(['confirmed', 'notconfirmed']),
        
        'provider_id'   =>  factory(App\Provider::class)
        
    ];
});
