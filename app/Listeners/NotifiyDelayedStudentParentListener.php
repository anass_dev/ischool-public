<?php

namespace App\Listeners;

use App\Events\StudentWasDelayedEvent;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\StudentDelayedMail;
use App\Notifications\StudentDelayed;
use App\Settings;

class NotifiyDelayedStudentParentListener implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  StudentWasDelayedEvent  $event
     * @return void
     */
    public function handle(StudentWasDelayedEvent $event)
    {
        $student = $event->delay->student;

        if(Settings::isEmailNotificationEnabled('delays')){

            Mail::to($student)->send(new StudentDelayedMail($event->delay));
            Mail::to($student->parent)->send(new StudentDelayedMail($event->delay,$student->parent));
        }

        if(Settings::isSMSNotificationEnabled('delays')){

            $student->notify( new StudentDelayed($event->delay));
            $student->parent->notify(new StudentDelayed($event->delay));
        }
    }
}
