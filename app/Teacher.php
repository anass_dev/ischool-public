<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];
    



    /**
     * 
     * create a one-to-one realtionship with teacher_salaries table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     * 
     */

    public function salary(){

        return $this->belongsTo('App\TeacherSalary','current_salary','id');

    }




    /**
     * 
     * create a one-to-many realtionship with teacher_salaries table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */

    public function salaries(){

        return $this->hasMany('App\TeacherSalary');

    }


    
 

}
