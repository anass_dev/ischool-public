<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class StudentParent extends Model
{

    use SoftDeletes;
    use Notifiable;
    
    protected $table = "parents";

    protected $guarded = [];

    /**
     * Route notifications for the Nexmo channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        //return $this->phone_number;
        return '212666673732';
    }


    /**
     * create a one-to-manu realtionship with students table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function students(){

        return $this->hasMany('App\Student','parent_id');
    }




}
