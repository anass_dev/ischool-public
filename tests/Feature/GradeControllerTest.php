<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Grade;
use Faker\Provider\Lorem;

class GradeControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_grade(){

        $this->get('/grades')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_grade(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->get('/grades')->assertStatus(200);

    }




    /** @test */
    public function show_interview_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $this->get('/grades/create')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_grade(){

       

        $this->actingAs(Factory(User::class)->create());
        $grade = factory(Grade::class)->create();

        $this->get('/grades/'.$grade->id)->assertStatus(200);

    }




    /** @test */
    public function a_grade_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/grades',[
            'name' => 'first',
        ]);

        $this->assertCount(1,Grade::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $this->post('/grades',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_grade(){

       

        $this->actingAs(Factory(User::class)->create());

        $grade = factory(Grade::class)->create();

        $this->get('/grades/'.$grade->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_grade_can_be_updated(){

        $this->withoutExceptionHandling();
        $this->actingAs(Factory(User::class)->create());

        $grade = Factory(Grade::class)->create();
    

        $this->patch('/grades/'.$grade->id,[
            'name'      => 'test_update',
        ]);

        $this->assertDatabaseHas('grades', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $grade = Factory(Grade::class)->create();
    
        $this->patch('/grades/'.$grade->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('grades', ['name'=>'first_test']);
    
    
    }




    /** @test */
    public function a_grade_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $grade = Factory(Grade::class)->create();
    
        $this->delete('/grades/'.$grade->id);

        $this->assertSoftDeleted('grades', ['id'=>$grade->id]);
        
        
    }
}
