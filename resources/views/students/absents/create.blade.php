@extends('layouts.app')
@section('content')
<div class="p-2">
     <!-- ===== Breadcrumb ===== -->
        
     <breadcrumb :items="[

        {'name':'{{ $student->name.' '.$student->surname }}', 'link':'{{ route('students.show',$student) }}' }, 
        {'name':'absents', 'link':'{{ route('students.absents.index',$student) }}' },
        {'name':'add', 'link':'' }
    
    ]"></breadcrumb>
</div>

    <div class="container p-2">

    
        <div class="border shadow-sm rounded-md">


            <div class="bg-gray-100 rounded-md">

                <form-header title="{{_('Add Absent') }}"></form-header>
                <info-person-bar :person="{{ $student }}"></info-person-bar>
    
            </div> 

            <form action="{{ route('students.absents.store',$student) }}" method="post">

                    <div class="border-t border-b py-8 px-4">

                            @csrf
                            
                            @include('students.absents.form')
                        
                    </div>

                    <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                        <back-button link="{{ route('students.absents.index',$student) }}" text="{{ _('Back') }}"></back-button>
                        <save-button text="Save"></save-button>
                        
                    </div>
            </form>

        </div>
    
        
    </div>

@endsection