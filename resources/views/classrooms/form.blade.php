<div>

    <div class="flex flex-wrap">

        <!--=== name ===-->

        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="name" class="text-md">{{ _('Name') }}</label>
    
                <am-input name="name" type="text" value="{{ $classroom->name ?? old('name') }}"></am-input>
                
                @error('name')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>


        <!--=== capacity ===-->

        <div class="w-full md:w-1/2">

            <div class="flex flex-col p-4">
                            
                <label for="capacity" class="text-md">{{ _('capacity') }}</label>
    
                <am-input name="capacity" type="number" value="{{ $classroom->capacity ?? old('capacity') }}"></am-input>
                
                @error('capacity')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>

    </div>


</div>