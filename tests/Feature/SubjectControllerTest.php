<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Subject;
use App\User;

class SubjectControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_subject(){

        $response = $this->get('/subjects')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_subject(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/subjects')->assertOk();

    }




    /** @test */
    public function add_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/subjects/create')->assertOk();

    }



    /** @test */
    public function subject_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        
        $this->post('/subjects',[
            'name'              => 'math',
            'description'       => 'subject math',
            'hours_number'      => 15,
        ]);

        $this->assertCount(1,Subject::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

       

        $this->actingAs(Factory(User::class)->create());

        $this->post('/subjects',[
            'name'      => '10',
            'occupied'  => 'else',

        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_subject(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $subject = factory(Subject::class)->create();

        $response = $this->get('/subjects/'.$subject->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_subject_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $subject = Factory(Subject::class)->create();
        
       

        $this->patch('/subjects/'.$subject->id,[
            'name'  => 'new_name',
            'description'      => 'subject math',
            'hours_number'  => 15,
        ]);

        $this->assertDatabaseHas('subjects', ['name'=>'new_name']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $subject = Factory(Subject::class)->create();
    
        $this->patch('/subjects/'.$subject->id,[

            'name'  => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore sequi voluptate veritatis optio ex voluptates magni qui similique nobis nemo, iusto a adipisci explicabo pariatur dolores exercitationem quo? Nihil, officiis!',
            'description'      => 'sqd',
            'hours_number'  => null,
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('subjects', ['name'=>'sqd']);

    
    }




    /** @test */
    public function a_subject_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $subject = Factory(Subject::class)->create();

    
        $this->delete('/subjects/'.$subject->id);
        $this->assertSoftDeleted('subjects', ['id'=>$subject->id]);
        
        
    }
}
