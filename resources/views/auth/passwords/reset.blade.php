
@extends('layouts.auth')

@section('content')



<div class="w-full flex flex-wrap">


    <div class="w-1/2 shadow-2xl">
        <img class="object-cover w-full h-screen hidden md:block" 
        src="https://images.unsplash.com/photo-1510070112810-d4e9a46d9e91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    </div>

  <!-- Login Section -->
  <div class="w-full md:w-1/2 flex flex-col">

        <div class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-24">
            <a href="#" class="bg-black text-white font-bold text-xl p-4">@include('layouts.main.svg-logo')</a>
        </div>

        <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
            
            <p class="text-center text-3xl">{{ __('Reset Password') }}</p>

            <form method="POST"  class="flex flex-col pt-2 md:pt-2" action="{{ route('password.update') }}">
                
               
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                 <div class="flex flex-col pt-4">
                    
   

                    <label for="email" class="text-lg">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" class="appearance-none border rounded w-full p-3 text-gray-700 mb-3 leading-tight focus:outline-none @error('email') border-red-500 @enderror" required autocomplete="email" autofocus >
                    
                    @error('email')
                        <span class="bg-red-100 border-l-2 border-red-500 my-2 text-red-700 p-2" role="alert">
                            <p>{{ $message }}</p>
                        </span>
                    @enderror
                
                </div>
                <div class="flex flex-col pt-4">
                    
                    <label for="password" class="text-lg">{{ __('Password') }}</label>
                    <input id="password" type="password" name="password" placeholder="password" class="appearance-none border rounded w-full p-3 text-gray-700 mb-3 leading-tight focus:outline-none @error('password') border-red-500 @enderror" autofocus >
                    
                    @error('password')
                        <span class="bg-red-100 border-l-2 border-red-500 my-2 text-red-700 p-2" role="alert">
                            <p>{{ $message }}</p>
                        </span>
                    @enderror
                    
                
                </div>

                <div class="flex flex-col pt-4">
                    
                    <label for="password-confirm" class="text-lg">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" name="password_confirmation" class="appearance-none border rounded w-full p-3 text-gray-700 mb-3 leading-tight focus:outline-none"  required autocomplete="new-password" >
                    
                </div>




                <input type="submit" value="{{ __('Reset Password') }}" class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8">


            </form>

        </div>    

  </div>           
</div>
@endsection









