<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Group name') }}</label>
                    <am-input name="name" type="text" value="{{ $group->name ?? old('name') }}"></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Grande') }}</label>
                    <drop :items="{{ $grades }}" name="grade" column_name="grade_id" selected={{ $group->grade_id ?? old('grade_id') }}></drop>
                    @error('grade_id')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>
    </div>
</div>