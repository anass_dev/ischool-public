<div>

    <div class="flex flex-wrap">

        <div class="w-full md:w-full">

            <div class="flex flex-col p-4">
                                
                <label for="name" class="text-md">{{ _('name') }}</label>
                <am-input name="name" type="text" value="{{ $debt->name ?? old('name') }}"  ></am-input>
                @error('name')

                    <alert-error message="{{ $message  }}"></alert-error>

                @enderror

            </div>

        </div>

    </div>




    <div class="flex flex-wrap">


        <!--=== amount ===-->

        <div class="w-full md:w-full">

            <div class="flex flex-col p-4">
                            
                <label for="amount" class="text-md">{{ _('Amount') }}</label>
    
                <am-input name="amount" type="number" value="{{ $debt->amount ?? old('amount') }}"  ></am-input>
                
                @error('amount')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
  

        
    </div>
    

</div>