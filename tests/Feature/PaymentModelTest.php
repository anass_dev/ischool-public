<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App;

class PaymentTest extends TestCase
{
    use RefreshDatabase;


    
    /**
     * 
     * tests if the paiment correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_paiment_is_saved(){

        
        $paiment = factory(App\Paiment::class)->create();


        $paiment->save();

        $this->assertDatabaseHas('paiments', ['id'=>$paiment->id]);


    }
    




    /**
     * 
     * tests if the paiment correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_paiment_is_updated(){
        
        $paiment = factory(App\Paiment::class)->create();
        
        $paiment->update(['amount' => 15131]);
        
        $this->assertDatabaseHas('paiments', ['amount' =>15131]);

    }




    /**
     * 
     * tests if the paiment correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_paiment_is_softDeleted(){

        $paiment = factory(App\Paiment::class)->create();

        $paiment->delete();

        $this->assertSoftDeleted($paiment);

    }




    /**
     * 
     * tests if the paiment correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_paiment_founde(){

        $paiment = factory(App\Paiment::class)->create();
        $paiment->save();


        $new_parent = App\Paiment::find($paiment->id);

        $this->assertEquals($paiment->id,$new_parent->id);

    }




    /**
     * 
     * tests the paiment paiments relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_paiment_student_relationship(){

        $paiment = factory(App\Paiment::class)->create();

        $student = factory(App\Student::class)->create();

        $paiment->student_id = $student->id;

        $paiment->save();


        $this->assertEquals($student->id,$paiment->student->id);


    }
    



}
