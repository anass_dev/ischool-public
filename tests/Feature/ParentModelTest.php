<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Tests\TestCase;
use App;

class ModelsTest extends TestCase
{

    use RefreshDatabase;



  
    /**
     * tests if the provider correctly inserted in a database
     *
     * @return void
     * 
     */

    public function test_parent_is_saved(){

        
        $parent = factory(App\StudentParent::class)->create();


        $parent->save();

        $this->assertDatabaseHas('parents', ['id'=>$parent->id]);


    }   




    /**
     * tests if the provider correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_parent_is_updated(){
        
        $parent = factory(App\StudentParent::class)->create();
        
        $parent->update(['email' => 'test@testemail.com']);
        
        $this->assertDatabaseHas('parents', ['email' =>'test@testemail.com']);


        
        
    }




    /**
     * tests if the provider correctly softdeleted in a database
     * 
     * @return void
     * 
     */


    public function test_parent_is_softDeleted(){

        $parent = factory(App\StudentParent::class)->create();

        $parent->delete();

        $this->assertSoftDeleted($parent);

    }





    /**
     * tests if the parent correctly fetched from a database
     * 
     * @return void
     * 
     */

    public function test_parent_founde(){

        $parent = factory(App\StudentParent::class)->create();
        $parent->save();


        $new_parent = App\StudentParent::find($parent->id);

        $this->assertEquals($parent->id,$new_parent->id);

    }




    /**
     * tests the parent student relationship 
     * 
     * @return void
     */

    public function test_parent_eleves(){

        $parent = factory(App\StudentParent::class)->create();
       
        $student = factory(App\Student::class)->make();
        
        $student->parent_id = $parent->id;
        $student->save();   

        $this->assertGreaterThan(0,$parent->students->count());

    }
}
