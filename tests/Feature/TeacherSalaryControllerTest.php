<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Teacher;
use App\TeacherSalary as Salary;

class TeacherSalaryControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function non_login_user_can_not_see_salaries(){

        $teacher = factory(Teacher::class)->create();
        $this->get('/teachers/'.$teacher->id.'/salaries')->assertRedirect('login');

    }




    /** @test */
    public function login_user_can_see_salaries(){

        $this->actingAs(factory(User::class)->create());

        $teacher = factory(Teacher::class)->create();
        $this->get('/teachers/'.$teacher->id.'/salaries')->assertStatus(200);

    }



    /** @test */
    public function create_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());
        $teacher = factory(Teacher::class)->create();

        $this->get('/teachers/'.$teacher->id.'/salaries/create')->assertStatus(200);

    }




    /** @test */
    public function single_result_show_view_is_not_accssible(){


        $this->actingAs(Factory(User::class)->create());

        $salary = factory(Salary::class)->create();
        $teacher   = factory(Teacher::class)->create();

        $this->get('/teachers/'.$teacher->id.'/salaries/'.$salary->id)->assertStatus(405);

    }



    /** @test */
    public function teacher_salary_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());


        $teacher       = factory(Teacher::class)->create();

        $this->post('/teachers/'.$teacher->id.'/salaries',[

                'amount'          => 8000,
                
        ])->assertRedirect('/teachers/'.$teacher->id.'/salaries');

        $this->assertCount(1,Salary::all());

    }





    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());


        $teacher       = factory(Teacher::class)->create();


        $this->post('/teachers/'.$teacher->id.'/salaries',[
            'amount'          => false,

        ])->assertSessionHasErrors();


    }




    /** @test */
    public function view_update_is_accssible_teachersalary(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());



        $salary = factory(Salary::class)->create();


        $this->get('/teachers/'.$salary->teacher_id.'/salaries/'.$salary->id.'/edit')->assertStatus(200);


    }




    /** @test */
    public function result_is_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $salary = Factory(Salary::class)->create();

        $this->patch('/teachers/'.$salary->teacher_id.'/salaries/'.$salary->id,[
            
            'amount'          => 8940,
            
        ]);

        $this->assertDatabaseHas('teacher_salaries', ['amount'=>8940]);


    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $salary = Factory(Salary::class)->create();

        $this->patch('/teachers/'.$salary->teacher_id.'/salaries/'.$salary->id,[
            
            'mark'=> '1ds',


        ])->assertSessionHasErrors();

    }


        /** @test */
    public function a_teachersalary_can_be_deleted(){

        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());


        $salary = Factory(Salary::class)->create();


        $this->delete('/teachers/'.$salary->teacher_id.'/salaries/'.$salary->id);


        $this->assertSoftDeleted('teacher_salaries', ['id'=>$salary->id]);
        
        
    }

}
