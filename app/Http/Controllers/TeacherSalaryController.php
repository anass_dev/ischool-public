<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\TeacherSalary as Salary;
use Illuminate\Http\Request;

class TeacherSalaryController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('teachers.salaries.index')->with(["salaries"=>Salary::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Teacher $teacher)
    {
        return view('teachers.salaries.create')->with(['teacher'=>$teacher]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Teacher $teacher)
    {
        $salary = Salary::make($this->validateRequest());
        $salary->teacher_id = $teacher->id;
        $salary->save();

        return redirect('teachers/'.$teacher->id.'/salaries');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher,Salary $salary)
    {
        view('teachers.salaries.edit')->with([

            "teacher"=> $teacher,
            "salary" => $salary
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher,Salary $salary)
    {
        $salary->update($this->validateRequest());

        return redirect('teachers/'.$teacher->id.'/salaries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher,Salary $salary)
    {
        $salary->delete();
    }

    /**
     * 
     * 
     */
     private function validateRequest(){

        return request()->validate([
            'amount'        => 'required|numeric'
        ]);

     }
}
