<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Name') }}</label>
                    <am-input name="name" type="text" value="{{ $subject->name ?? old('name') }}"  ></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="description" class="text-md">{{ _('Description') }}</label>
                    <am-textarea element_name="description" type="text" >{{ $subject->description ?? old('description') }}</am-textarea>
                    @error('description')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="hours_number" class="text-md">{{ _('Number of hours') }}</label>
                    <am-input name="hours_number" type="number" value="{{ $subject->hours_number ?? old('hours_number') }}"  ></am-input>
                    @error('hours_number')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


</div>