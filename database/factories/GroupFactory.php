<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Group;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker) {
    return [

        'name'      =>  $faker->word(),
        'standard'  =>  $faker->boolean($chanceOfGettingTrue = 70),
        'grade_id'  =>  Factory(App\Grade::class),

    ];
});
