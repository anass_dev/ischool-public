<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Exam;
use App\Subject;
use App\ExamType;

class ExamControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_exam(){

        $response = $this->get('/exams')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_exam(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/exams')->assertStatus(200);

    }




    /** @test */
    public function show_interview_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/exams/create')->assertStatus(200);

    }




    /** @test */
    public function view_create_is_accssible_exam(){

       

        $this->actingAs(Factory(User::class)->create());
        $exam = factory(Exam::class)->create();

        $response = $this->get('/exams/'.$exam->id)->assertStatus(200);

    }




    /** @test */
    public function a_exam_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $subject = factory(Subject::class)->create();
        $type = factory(ExamType::class)->create();

        $response = $this->post('/exams',[

            'name'=> 'exam__01',
            'subject_id'=>$subject->id,
            'exam_type_id'=> $type->id,
            

        ]);

        $this->assertCount(1,Exam::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $response = $this->post('/exams',[
                'name'=> false,
                'subject_id'=>'51450dd',
                'exam_type_id'=> 'notvalid',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_exam(){

       

        $this->actingAs(Factory(User::class)->create());

        $exam = factory(Exam::class)->create();

        $response = $this->get('/exams/'.$exam->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_exam_can_be_updated(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $exam = Factory(Exam::class)->create();
    
        $response = $this->patch('/exams/'.$exam->id,[
            
            'name'=> 'new_name_exam_01',
            'subject_id'=>$exam->subject_id,
            'exam_type_id'=> $exam->exam_type_id,
            
        ]);

        $this->assertDatabaseHas('exams', ['name'=>'new_name_exam_01']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $exam = Factory(Exam::class)->create();
    
        $response = $this->patch('/exams/'.$exam->id,[
            
            'name'=> 'new_name_exam_01',
     
            'exam_type_id'=> $exam->exam_type_id,
    
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('exams', ['name'=>'new_name_exam_01']);
    
    
    }




    /** @test */
    public function a_exam_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $exam = Factory(Exam::class)->create();
    
        $response = $this->delete('/exams/'.$exam->id);

        $this->assertSoftDeleted('exams', ['id'=>$exam->id]);
        
        
    }
}
