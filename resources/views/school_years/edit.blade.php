@extends('layouts.app')
@section('content')

       
<div class="container p-2">

    <!-- ===== Breadcrumb ===== -->
    
    <breadcrumb :items="[

        {'name':'school years', 'link':'{{ route('schoolyears.index') }}' }, 
        {'name':'edit', 'link':'' }
    
    ]"></breadcrumb>

    <div class="mt-4 border shadow-sm rounded-md">


        <div class="py-3 px-3 bg-gray-100 rounded-md">

        <div>
            <back-button link="{{ route('schoolyears.index') }}" text="{{ _('Back') }}"></back-button>
        </div>
        

        <div class="text-2xl my-3  mx-1 block text-gray-700"> {{_('Edit school year') }}  </div>

             
    </div>


    <form action="{{ route('schoolyears.update',$school_year) }}" method="post">

            <div class="border-t border-b py-8 px-4">

                    @csrf
                    @method('PUT')
                    @include('school_years.form')
                
            </div>

            <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                <back-button link="{{ route('schoolyears.index') }}" text="{{ _('Back') }}"></back-button>
                <save-button text="Save"></save-button>
                
            </div>
    </form>

</div>

</div>



@endsection