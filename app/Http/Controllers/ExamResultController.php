<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamResult;
use App\Student;

use Illuminate\Http\Request;

class ExamResultController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Exam $exam)
    {
        return view('exams.results.index')->with(["results"=>$exam->results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Exam $exam)
    {
        return view('exams.results.create')->with(["exam"=>$exam,"students"=>Student::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Exam $exam)
    {
        $result = ExamResult::make($this->validateRequest());
        $result->exam_id = $exam->id;
        $result->save();

        return redirect('exams/'.$exam->id.'/results');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam,ExamResult $result)
    {
        return view('exams.results.edit')->with([
            "exam"      => $exam,
            "result"    => $result,
            "students"  => Student::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam,ExamResult $result)
    {

        $result->update($this->validateRequest());     
        return redirect('exams/'.$exam->id.'/results');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam,ExamResult $result)
    {
        $result->delete();
    }

    /**
     * Validate the request
     * @return 
     */
    public function validateRequest(){

        return request()->validate([

            'student_id'=> 'required|integer',
            'mark'      => 'required|numeric|between:0,20'

        ]);

    }

}
