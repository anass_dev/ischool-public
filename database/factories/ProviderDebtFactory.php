<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProviderDebt;
use Faker\Generator as Faker;

use Illuminate\Support\Str;

$factory->define(ProviderDebt::class, function (Faker $faker) {
    return [

        'name'          =>  $faker->word(),
        'amount'        =>  $faker->randomFloat(2,2500,10500),
        'resolved'      =>  $faker->boolean($chanceOfGettingTrue = 70),
        'provider_id'   =>  factory(App\Provider::class)
    ];
});
