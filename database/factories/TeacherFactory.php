<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use Faker\Generator as Faker;

use Illuminate\Support\Str;


$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'name'          =>  $faker->firstName(),
        'surname'       =>  $faker->lastName(),

        'cin'           =>  $faker->randomElement(['R', 'RB','G']).$faker->numberBetween(10000, 100000),
        'sex'           =>  $faker->randomElement(['m', 'f']),
        
        'phone_number'  =>  $faker->e164PhoneNumber(),
        'email'         =>  $faker->safeEmail(),

        'birthday'      =>  $faker->dateTimeBetween('-90 years','-30 years'),
        'address'       =>  $faker->address(),
    ];
});
