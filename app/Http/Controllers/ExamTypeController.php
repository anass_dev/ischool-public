<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ExamType;

class ExamTypeController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exams.types.index')->with(["types"=>ExamType::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('exams.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ExamType::create($this->validateRequest());
        return redirect('examtypes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamType $examtype)
    {
        return view('exams.types.edit')->with(["type"=>$examtype]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamType $examtype)
    {
        $examtype->update($this->validateRequest());
        return redirect('examtypes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamType $examtype)
    {
       $examtype->delete();
    }
    
    /**
     * 
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([

            'name'=>'required|string|max:100'

        ]);

    }
}
