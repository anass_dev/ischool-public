<div>

    <div class="flex flex-wrap">

        <div class="w-full md:w-full">
            <div class="flex flex-col p-4">
                                
                <label for="name" class="text-md">{{ _('name') }}</label>
                <am-input name="name" type="text" value="{{ $delay->name ?? old('name') }}"  ></am-input>
                @error('name')

                    <alert-error message="{{ $message  }}"></alert-error>

                @enderror

            </div>
        </div>

    

    </div>




    <div class="flex flex-wrap">


        <!--=== Description ===-->

        <div class="w-full md:w-full">

            <div class="flex flex-col p-4">
                            
                <label for="description" class="text-md">{{ _('Description') }}</label>
    
                <am-textarea element_name="description" type="text" >{{ $delay->description ?? old('description') }}</am-textarea>
                
                @error('description')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        

    </div>

    <div class="flex flex-wrap">


        <!--=== Duration ===-->

        <div class="w-full md:w-full">

            <div class="flex flex-col p-4">
                            
                <label for="duration" class="text-md">{{ _('Duration ( by minutes ) ') }}</label>
    
                <am-input name="duration" type="number" value="{{ $result->mark ?? old('mark') }}"  ></am-input>
                
                @error('duration')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        

    </div>
    

</div>