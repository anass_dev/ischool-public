<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exam;
use App\Grade;
use App\ExamType;
use App\Subject;


class ExamController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exams.index')->with(['exams'=>Exam::all(['id','name','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exams.create')->with(["exam_types"=>ExamType::all(),"subjects"=>Subject::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exam = Exam::create($this->validateRequest());
        return redirect('exams/'.$exam->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        $results   = $exam->results()->get(['id','mark','student_id']);
        

        foreach ($results as $index=>$result){

            $result->student = $result->student->surname . ' ' . $result->student->name;
            unset($result->student_id);

            
            $results->{$index} = $result;
            
        }
   
       
        return view('exams.show')->with(["exam"=>$exam,"results"=>$results]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        return view('exams.edit')->with([

            "exam_types" => ExamType::all(),
            "subjects"   => Subject::all(),
            "exam"       => $exam

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        $exam->update($this->validateRequest());
        return redirect('exams/'.$exam->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        $exam->delete();
        
    }

    /**
     * 
     * 
     * 
     */
    private function validateRequest(){
        return request()->validate([
            
                'name'          =>'required|max:100',
                'subject_id'    =>'required|numeric',
                'exam_type_id'  =>'required|numeric',

        ]);
    }
}
