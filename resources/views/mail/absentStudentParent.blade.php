@component('mail::message')

Hi {{ $parent->name }} {{ $parent->surname }},

we notify you that {{ $absent->student->name }} {{ $absent->student->surname }} was absent from {{ $absent->from }} to {{ $absent->to }}.

@if(trim($absent->description) != '')
more information:
{{ $absent->description }}
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent