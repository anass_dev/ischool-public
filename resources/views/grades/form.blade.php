<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('name') }}</label>
                    <am-input name="name" type="text" value="{{ $grade->name ?? old('name') }}"></am-input>
                    @error('name')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>


</div>