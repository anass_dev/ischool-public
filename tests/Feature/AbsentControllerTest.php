<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Absent;
use App\Student;



class AbsentControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_absent(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/absents')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_absent(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/absents')->assertStatus(200);
    }




    /** @test */
    public function view_create_is_accssible_absent(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/absents/create')->assertStatus(200);

    }




    /** @test */
    public function a_absent_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/absents',[
            'name'=> 'absent_name',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        ]);

        $this->assertCount(1,Absent::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/absents',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_absent(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $absent = factory(Absent::class)->create();


        $this->get('/students/'.$absent->student_id.'/absents/'.$absent->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_absent_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $absent = Factory(Absent::class)->create();
    

        $this->patch('/students/'.$absent->student_id.'/absents/'.$absent->id,[
            'name'      => 'test_update',
        ]);

        $this->assertDatabaseHas('absents', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $absent     = Factory(Absent::class)->create();
    
        $this->patch('/students/'.$absent->student_id.'/absents/'.$absent->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('absents', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_absent_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $absent = Factory(Absent::class)->create();
    
        $this->delete('/students/'.$absent->student_id.'/absents/'.$absent->id);
        

        $this->assertSoftDeleted('absents', ['id'=>$absent->id]);
        
    }
}
