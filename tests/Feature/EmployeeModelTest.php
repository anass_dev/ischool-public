<?php

namespace Tests\Feature;

use Tests\TestCase;
use App;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;



class EmployeeTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * 
     * tests if the Employee correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_employee_is_saved(){

        
        $Employee = factory(App\Employee::class)->create();


        $Employee->save();

        $this->assertDatabaseHas('employees', ['id'=>$Employee->id]);


    }
    
    /**
     * 
     * tests if the Employee correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_employee_is_updated(){
        
        $Employee = factory(App\Employee::class)->create();
        
        $Employee->update(['email' => 'test@testemail.com']);
        
        $this->assertDatabaseHas('employees', ['email' =>'test@testemail.com']);

    }




    /**
     * 
     * tests if the Employee correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_employee_is_softDeleted(){

        $Employee = factory(App\Employee::class)->create();

        $Employee->delete();

        $this->assertSoftDeleted($Employee);

    }




    /**
     * 
     * tests if the Employee correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_employee_founde(){

        $Employee = factory(App\Employee::class)->create();
        $Employee->save();


        $new_employee = App\Employee::find($Employee->id);

        $this->assertEquals($Employee->id,$new_employee->id);

    }

}
