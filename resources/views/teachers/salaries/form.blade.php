<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="amount" class="text-md">{{ _('Amount') }}</label>
                    <am-input name="amount" type="number" ></am-input>
                    @error('amount')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>



</div>