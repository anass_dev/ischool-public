@component('mail::message')

Hi {{ $absent->student->name }} {{ $absent->student->surname }},

we notify you that a absent is mark form {{ $absent->from }} to {{ $absent->to }}.

@if(trim($absent->description) != '')
more information:<br>
{{ $absent->description }}
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent