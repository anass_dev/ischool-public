<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Delay;
use App\Student;

class DelayControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_delay(){

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/delays')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_delay(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/delays')->assertStatus(200);
    }




    /** @test */
    public function view_create_is_accssible_delay(){

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();
        $this->get('/students/'.$student->id.'/delays/create')->assertStatus(200);

    }




    /** @test */
    public function a_delay_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();


        $this->post('/students/'.$student->id.'/delays',[
            'name'=> 'delay_name',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'duration' => 10
        ]);

        $this->assertCount(1,Delay::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());

            $student = factory(Student::class)->create();
    
            $this->post('/students/'.$student->id.'/delays',[
                'name' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque consectetur tenetur sapiente culpa facilis eaque maxime ducimus quis autem iste voluptatum, veniam laborum ea itaque atque cum! Placeat, dolor aut.lorem',
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_delay(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $delay = factory(Delay::class)->create();


        $this->get('/students/'.$delay->student_id.'/delays/'.$delay->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_delay_can_be_updated(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $delay = Factory(Delay::class)->create();
    

        $this->patch('/students/'.$delay->student_id.'/delays/'.$delay->id,[
            'name'      => 'test_update',
            'duration'  => 5
        ]);

        $this->assertDatabaseHas('delays', ['name'=>'test_update']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $delay     = Factory(Delay::class)->create();
    
        $this->patch('/students/'.$delay->student_id.'/delays/'.$delay->id,[
            
            'name'      => '',
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('delays', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_delay_can_be_deleted(){


        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());



        $delay = Factory(Delay::class)->create();
    
        $this->delete('/students/'.$delay->student_id.'/delays/'.$delay->id);
        

        $this->assertSoftDeleted('delays', ['id'=>$delay->id]);
        
    }
}
