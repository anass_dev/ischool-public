<?php

namespace App\Notifications;

use App\Absent;
use App\Student;

use Illuminate\Bus\Queueable;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

use Illuminate\Notifications\Notification;

use App\Settings;

class StudentAbsentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $absent;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Absent $absent)
    {
        $this->absent = $absent;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return  ['nexmo'];
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {

        $message = Settings::absentSMSMessage($this->absent);
        
        return (new NexmoMessage)->content($message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
