<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Student;
use App\StudentParent;
use App\Grade;

class StudentControllerTest extends TestCase
{

    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_student(){

        $response = $this->get('/students')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_student(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/students')->assertOk();

    }




    /** @test */
    public function show_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/students/create')->assertOk();

    }




    /** @test */
    public function student_view_create_is_accssible(){

       

        $this->actingAs(Factory(User::class)->create());
        $student = factory(Student::class)->create();

        $response = $this->get('/students/'.$student->id)->assertOk();

    }




    /** @test */
    public function student_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $parnet = factory(StudentParent::class)->create();

        
        $this->post('/students',[

            'name'          =>  'Parentname',
            'surname'       =>  'Parentsurname',

            'cin'           =>  'R346885',
            'sex'           =>  'f',
            
            'phone_number'  =>  '0661648497',
            'email'         =>  'email@email.com',

            'birthday'      =>  '1990-01-01',
            'address'       =>  'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.',
            'grade_id'      =>  factory(Grade::class)->create()->id,
            'parent_id'    =>  $parnet->id 

        ]);

        $this->assertCount(1,Student::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/students',[
            'name'          => 'me',
            'surname'       =>'',

            'cin'           => '44',
            'sex'           =>'',
            
            'phone_number'  =>'0667',
            'email'         =>'email@emailcom',

            'birthday'      =>'19f0-01-01',
            'address'       =>'' 

        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_student(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $student = factory(Student::class)->create();

        $response = $this->get('/students/'.$student->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_student_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $student = Factory(Student::class)->create();
    
        $this->patch('/students/'.$student->id,[

                'name'          => 'Parentname',
                'surname'       =>'Parentsurname',
    
                'cin'           => 'R346885',
                'sex'           =>'f',
                            
                'phone_number'  =>'0661648497',
                'email'         =>'email@email.com',
    
                'birthday'      =>  '1990-01-01',
                'grade_id'      =>  $student->grade_id,
                'parent_id'     =>  $student->parent_id ,
                'address'       =>  'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 
    
        ]);

        $this->assertDatabaseHas('students', ['surname'=>'Parentsurname']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $student = Factory(Student::class)->create();
    
        $this->patch('/students/'.$student->id,[

                'name'          => 'P',
                'surname'       =>'Parname',
                'cin'           => 'R85',
                'sex'           =>'', 
                'phone_number'  =>'97',
                'email'         =>'email@eom',
                'birthday'      =>'m0-01-01',
                'address'       =>'Lore enim culpa ipsa laborum.'
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('students', ['surname'=>'Parname']);

    
    }




    /** @test */
    public function a_student_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $student = Factory(Student::class)->create();

    
        $this->delete('/students/'.$student->id);
        $this->assertSoftDeleted('students', ['id'=>$student->id]);
        
        
    }
}
