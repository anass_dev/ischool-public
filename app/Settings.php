<?php

namespace App;

use Storage;

class Settings
{

    public static function params() {

        return  json_decode( Storage::get('settings.json') );
        
    }

    public static function isEmailNotificationEnabled($action) {

        if($params = json_decode( Storage::get('settings.json') )) {

            return $params->{$action}->email->enabled;
        }

        return false;
        
    }

    public static function isSMSNotificationEnabled($action) {

        if($params = json_decode( Storage::get('settings.json') )) {

            return $params->{$action}->sms->enabled;
        }

        return false;
        
    }

    public static function delaySMSMessage($delay) {

        
        if($params = json_decode( Storage::get('settings.json') )) {

            $message = $params->delays->sms->message;

            $message = str_replace("[student_name]",$delay->student->name . ' ' . $delay->student->surname,$message);
            $message = str_replace("[parent_name]",$delay->student->parent->name . ' ' . $delay->student->parent->surname,$message);
            $message = str_replace("[duration]",$delay->duration,$message);


            return $message;
        }

        return "Student {$delay->student->name} {$delay->student->surname} was delayd for {$delay->duration}. ";
        
    }

    public static function absentSMSMessage($absent) {

        
        if($params = json_decode( Storage::get('settings.json') )) {

            $message = $params->absents->sms->message;

            $message = str_replace("[student_name]",$absent->student->name . ' ' . $absent->student->surname,$message);
            $message = str_replace("[parent_name]",$absent->student->parent->name . ' ' . $absent->student->parent->surname,$message);
            
            $message = str_replace("[from]",$absent->from,$message);
            $message = str_replace("[to]",$absent->to,$message);


            return $message;
        }

        return "Student {$absent->student->name} {$absent->student->surname} was absent from {$absent->from} to {$absent->to}.";
        
    }
}
