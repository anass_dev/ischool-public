<?php

namespace App\Listeners;

use App\Events\StudentWasAbsentEvent;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\StudentAbsentMail;
use App\Notifications\StudentAbsentNotification;
use App\Settings;

class NotifiyAbsentStudentParentListener implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  StudentWasAbsentEvent  $event
     * @return void
     */
    public function handle(StudentWasAbsentEvent $event)
    {

        $student = $event->absent->student;

        if(Settings::isEmailNotificationEnabled('absents')){

            Mail::to($student)->send(new StudentAbsentMail($event->absent));
            Mail::to($student->parent)->send(new StudentAbsentMail($event->absent));
        }

        if(Settings::isSMSNotificationEnabled('absents')){

            $student->notify( new StudentAbsentNotification($event->absent));
            $student->parent->notify(new StudentAbsentNotification($event->absent));
        }
       
    }
}
