<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/
 
Auth::routes(['register' => true]);

/*
|--------------------------------------------------------------------------
| Dashbord & Options Routes
|--------------------------------------------------------------------------
*/

Route::redirect('/',        '/dashboard', 301);
Route::redirect('/home',    '/dashboard', 301);

Route::get('/dashboard',    'HomeController@index'      )->name('home');
Route::get('/settings',     'SettingController@index'   )->name('settings');
Route::post('/settings',    'SettingController@update'  )->name('update.settings');

/*
|--------------------------------------------------------------------------
| Student & Parent routes
|--------------------------------------------------------------------------
*/

Route::resource('parents',      'ParentController');

Route::resource('students',     'StudentController');

Route::resource('students.paiments',    'PaimentController'  );
Route::resource('students.debts',       'DebtController'     );
Route::resource('students.absents',     'AbsentController'   )->except('show');
Route::resource('students.delays',      'DelayController'    )->except('show');
Route::resource('students.complaints',  'ComplaintController')->except('show');
Route::resource('students.notes',       'NoteController'     )->except('show');

/*
|--------------------------------------------------------------------------
| School elements routes
|--------------------------------------------------------------------------
*/

Route::resource('grades',       'GradeController');
Route::resource('schoolyears',  'SchoolyearController');

Route::resource('classrooms',   'ClassroomController'   );
Route::resource('subjects',     'SubjectController'     )->except('show');

Route::resource('exams',        'ExamController'        );
Route::resource('exams.results','ExamResultController'  )->except('show');
Route::resource('examtypes',    'ExamTypeController'    )->except('show');


/*
|--------------------------------------------------------------------------
|  Group Elements
|--------------------------------------------------------------------------
|*/

Route::resource('groups'         ,'GroupController'       );
Route::resource('groups.courses' ,'CourseController'      )->except(['create','show','edit']);


Route::get('groups/{group}/students','GroupController@addStudents')->name('groups.students.add');
Route::post('groups/{group}/students','GroupController@storeStudents')->name('groups.students.store');




/*
|--------------------------------------------------------------------------
| School Employees & teachers routes
|--------------------------------------------------------------------------
*/

Route::resource('teachers',     'TeacherController');
Route::resource('teachers.salaries',    'TeacherSalaryController')->except('show');


Route::resource('employees',    'EmployeeController');
Route::resource('employees.salaries',    'EmployeeSalaryController')->except('show');



/*
|--------------------------------------------------------------------------
| School providers routs
|--------------------------------------------------------------------------
*/

Route::resource('providers',    'ProviderController');

Route::resource('providers.paiments',    'ProviderPaimentController' );
Route::resource('providers.debts',       'ProviderDebtController'    );
