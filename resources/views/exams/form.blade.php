<div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-full">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="name" class="text-md">{{ _('Name') }}</label>
                    <am-input name="name" type="text" value="{{ $exam->name ?? old('name') }}"></am-input>
                    @error('name')

                            <alert-error  message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>

    <div class="flex flex-wrap">


        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="description" class="text-md">{{ _('Description') }}</label>
                    <drop :items="{{ $exam_types }}" name="exam_type" column_name="exam_type_id"  selected={{ $exam->sex ?? old('sex') }}></drop>
                    @error('exam_type_id')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

        <div class="w-full md:w-1/2">
                <div class="flex flex-col p-4 ">
                                    
                    <label for="description" class="text-md">{{ _('Description') }}</label>
                    <drop :items="{{ $subjects }}" name="subject" column_name="subject_id"  selected={{ $exam->subject_id ?? old('subject_id') }}></drop>
                    @error('subject_id')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>
        </div>

    </div>



</div>