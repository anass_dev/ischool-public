<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

use App\Grade;
use App\StudentParent;
use App\Student;


use Illuminate\Support\Str;


$factory->define(Student::class, function (Faker $faker) {
    return [
        'name'          =>  $faker->firstName(),
        'surname'       =>  $faker->lastName(),

        'cin'           =>  $faker->randomElement(['R', 'RB','G']) . Str::random(10),
        'sex'           =>  $faker->randomElement(['m', 'f']),
        'phone_number'  =>  $faker->e164PhoneNumber(),
        'email'         =>  $faker->safeEmail(),
        'birthday'      =>  $faker->dateTimeBetween('-19 years','-11 years'),
        'address'       =>  $faker->address(),
      
        'grade_id'      => factory(Grade::class)->create()->id,
        
        'parent_id'     => factory(StudentParent::class)
            
    ];
});
