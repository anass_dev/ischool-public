<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Absent;
use App\Student;
use Faker\Generator as Faker;

$factory->define(Absent::class, function (Faker $faker) {
    return [
                 
        'name'          =>  $faker->word(),
        'resolved'      =>  $faker->boolean($chanceOfGettingTrue = 70),
        'description'   =>  $faker->realText(),
        'from'          =>  $faker->dateTimeBetween('-8 days','-4 days'),
        'to'            =>  $faker->dateTimeBetween('-4 days','-1 days'),
        'student_id'    =>  factory(Student::class)
    ];
});
