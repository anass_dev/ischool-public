<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Teacher;

class TeacherControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_teacher(){

        $response = $this->get('/teachers')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_teacher(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/teachers')->assertOk();

    }




    /** @test */
    public function show_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/teachers/create')->assertOk();

    }




    /** @test */
    public function teacher_view_create_is_accssible(){

       

        $this->actingAs(Factory(User::class)->create());
        $teacher = factory(Teacher::class)->create();

        $response = $this->get('/teachers/'.$teacher->id)->assertOk();

    }




    /** @test */
    public function teacher_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        
        $this->post('/teachers',[

            'name'          =>  'Parentname',
            'surname'       =>  'Parentsurname',

            'cin'           =>  'R346885',
            'sex'           =>  'f',
            
            'phone_number'  =>  '0661648497',
            'email'         =>  'email@email.com',

            'birthday'      =>  '1990-01-01',
            'address'       =>  'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.',

        ]);

        $this->assertCount(1,Teacher::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/teachers',[
            'name'          => 'me',
            'surname'       =>'',

            'cin'           => '44',
            'sex'           =>'',
            
            'phone_number'  =>'0667',
            'email'         =>'email@emailcom',

            'birthday'      =>'19f0-01-01',
            'address'       =>'' 

        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_teacher(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $teacher = factory(Teacher::class)->create();

        $response = $this->get('/teachers/'.$teacher->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_teacher_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $teacher = Factory(Teacher::class)->create();
    
        $this->patch('/teachers/'.$teacher->id,[

                'name'          => 'Parentname',
                'surname'       =>'Parentsurname',
    
                'cin'           => 'R346885',
                'sex'           =>'f',
                            
                'phone_number'  =>'0661648497',
                'email'         =>'email@email.com',
    
                'birthday'      =>  '1990-01-01',
                'address'       =>  'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 
    
        ]);

        $this->assertDatabaseHas('teachers', ['surname'=>'Parentsurname']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $teacher = Factory(Teacher::class)->create();
    
        $this->patch('/teachers/'.$teacher->id,[

                'name'          => 'P',
                'surname'       =>'Parname',
                'cin'           => 'R85',
                'sex'           =>'', 
                'phone_number'  =>'97',
                'email'         =>'email@eom',
                'birthday'      =>'m0-01-01',
                'address'       =>'Lore enim culpa ipsa laborum.'
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('teachers', ['surname'=>'Parname']);

    
    }




    /** @test */
    public function a_teacher_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $teacher = Factory(Teacher::class)->create();

    
        $this->delete('/teachers/'.$teacher->id);
        $this->assertSoftDeleted('teachers', ['id'=>$teacher->id]);
        
        
    }
}
