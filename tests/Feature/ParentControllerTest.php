<?php

namespace Tests\Feature;

use App\StudentParent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;


class ParentControllerTest extends TestCase
{

    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_parent(){

        $response = $this->get('/parents')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_parent(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/parents')->assertOk();

    }




    /** @test */
    public function show_interview_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/parents/create')->assertOk();

    }




    /** @test */
    public function view_create_is_accssible_parent(){

       

        $this->actingAs(Factory(User::class)->create());
        $parent = factory(StudentParent::class)->create();

        $response = $this->get('/parents/'.$parent->id)->assertOk();

    }




    /** @test */
    public function a_parent_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $response = $this->post('/parents',[
            'name'=> 'Parentname',
            'surname'=>'Parentsurname',

            'cin'=> 'R346885',
            'sex'=>'f',
            
            'phone_number'=>'0661648497',
            'email'=>'email@email.com',

            'birthday'=>'1990-01-01',
            'address'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 

        ]);

        $this->assertCount(1,StudentParent::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $response = $this->post('/parents',[
                'name'=> 'me',
                'surname'=>'',
    
                'cin'=> '44',
                'sex'=>'',
                
                'phone_number'=>'0667',
                'email'=>'email@emailcom',
    
                'birthday'=>'19f0-01-01',
                'address'=>'' 
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_parent(){

       

        $this->actingAs(Factory(User::class)->create());

        $parent = factory(StudentParent::class)->create();

        $response = $this->get('/parents/'.$parent->id.'/edit')->assertOk();

    }




    /** @test */
    public function a_parent_can_be_updated(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $parent = Factory(StudentParent::class)->create();
    
        $response = $this->patch('/parents/'.$parent->id,[
                'name'=> 'Parentname',
                'surname'=>'Parentsurname',
    
                'cin'=> 'R346885',
                'sex'=>'f',
                            
                'phone_number'=>'0661648497',
                'email'=>'email@email.com',
    
                'birthday'=>'1990-01-01',
                'address'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cum provident vitae eaque ut tempora nihil, enim culpa ipsa laborum.' 
    
        ]);

        $this->assertDatabaseHas('parents', ['surname'=>'Parentsurname']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $parent = Factory(StudentParent::class)->create();
    
        $response = $this->patch('/parents/'.$parent->id,[
                'name'=> 'P',
                'surname'=>'Parname',
    
                'cin'=> 'R85',
                'sex'=>'',
                            
                'phone_number'=>'97',
                'email'=>'email@eom',
    
                'birthday'=>'m0-01-01',
                'address'=>'Lore enim culpa ipsa laborum.' 
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('parents', ['surname'=>'Parname']);
    
    
    }




    /** @test */
    public function a_parent_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $parent = Factory(StudentParent::class)->create();
    
        $response = $this->delete('/parents/'.$parent->id);

        $this->assertSoftDeleted('parents', ['id'=>$parent->id]);
        
        
    }
    
}
