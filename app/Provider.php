<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{

    use SoftDeletes;

    protected $guarded = [];




    /**
     * create a one-to-many realtionship with paiments table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */

    public function paiments(){

        return $this->hasMany('App\ProviderPaiment');
    }
    


    
    /**
     * create a one-to-many realtionship with debts table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */

    public function debts(){

        return $this->hasMany('App\ProviderDebt');
    }

}
