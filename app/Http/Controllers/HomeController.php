<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Student;
use App\StudentParent;
use App\Teacher;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')->with([
            "teachers"      => Teacher::all()->count(),
            "classrooms"    => Classroom::all()->count(),
            "parents"       => StudentParent::all()->count(),
            "students"      => Student::all()->count()
        ]);
    }
}
