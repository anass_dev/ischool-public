<div>

    <div class="flex flex-wrap">

        <div class="w-full md:w-full">
            <div class="flex flex-col p-4">
                                
                <label for="name" class="text-md">{{ _('name') }}</label>
                <am-input name="name" type="text" value="{{ $absent->name ?? old('name') }}"  ></am-input>
                @error('name')

                    <alert-error message="{{ $message  }}"></alert-error>

                @enderror

            </div>
        </div>

    

    </div>




    <div class="flex flex-wrap">


        <!--=== cin ===-->

        <div class="w-full md:w-full">

            <div class="flex flex-col p-4">
                            
                <label for="description" class="text-md">{{ _('Description') }}</label>
    
                <am-textarea element_name="description" type="text" >{{ $absent->description ?? old('description') }}</am-textarea>
                
                @error('description')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
  

        
    </div>

    <div class="flex flex-wrap justify-center">

       

        
        <div class="w-full md:w-1/4">

            <div class="flex flex-col p-4">
                            
                <label for="from" class="text-md">{{ _('From') }}</label>
    
                <am-input name="from" type="date" value="{{ $absent->from ?? old('from') }}"  ></am-input>
                
                @error('from')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>
        
        <!--=== PhoneNumber ===-->

        <div class="w-full md:w-1/4">

            <div class="flex flex-col p-4">
                            
                <label for="to" class="text-md">{{ _('to') }}</label>
    
                <am-input name="to" type="date" value="{{ $absent->to ?? old('to') }}"  ></am-input>
                
                @error('to')
    
                    <alert-error message="{{ $message  }}"></alert-error>
    
                @enderror
    
            </div>

        </div>



    </div>



</div>