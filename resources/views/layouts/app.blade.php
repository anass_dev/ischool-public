<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <head>

        @include('layouts.partials.header')

    </head>

    <body>


        <div id="app">
            
            @include('layouts.partials.nav')

            <side-menu></side-menu>

            <div class="container mx-auto mt-16">

                @yield('content')

            </div>



            <footer></footer>

        </div>



        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        
        <script src="{{ url('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ url('js/sweetalert2.min.js') }}"></script>

    </body>
</html>
