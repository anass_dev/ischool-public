@extends('layouts.app')
@section('content')


<div class="p-2">
    <!-- ===== Breadcrumb ===== -->

    <breadcrumb :items="[

    {'name':'Grades', 'link':'' },

    ]"></breadcrumb>
</div>
 
<div class="container p-2">

        
     
    <div class="flex justify-between py-2 mb-2 rounded items-center">
        <div class="text-2xl text-gray-800 flex">

                <svg class="fill-current text-gray-400 w-10 mx-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                    <path d="M6 3l-6 8h4v10h4v-10h4l-6-8zm16 6h-8v-2h8v2zm2-6h-10v2h10v-2zm-4 8h-6v2h6v-2zm-2 4h-4v2h4v-2zm-2 4h-2v2h2v-2z"/>                </svg>

                {{_('Grades') }}
            
            </div>
            <div class=""> <success-button text="{{ _('Add grade') }}" link="{{ route('grades.create') }}"></success-button> </div>
        </div>

        
        <am-table :items="{{ $grades }}" index_link="{{ route('grades.index') }}"></am-table>
        
    </div>


@endsection