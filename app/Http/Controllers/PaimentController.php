<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Paiment;

class PaimentController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $student)
    {
        return view('students.paiments.index')->with(['paiments'=> $student->paiments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $student)
    {
        return view('students.paiments.create')->with(['student'=> $student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $student)
    {
        $paiment  = Paiment::make($this->validateRequest());
        $paiment->student_id = $student->id;
        $paiment->save();

        return redirect('/students/'.$student->id.'/paiments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,Paiment $paiment)
    {
        return view('students.paiments.edit')->with(['paiment'=>$paiment,"student"=>$student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Student $student,Paiment $paiment)
    {
        $paiment->update($this->validateRequest());

        return redirect('/students/'.$student->id.'/paiments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student,Paiment $paiment)
    {
        $paiment->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'      => 'required|max:100',
            'method'    => 'required|max:100',
            'amount'    => 'required|numeric',
        ]);
    } 
}
