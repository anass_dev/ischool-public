<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Grade::class,10)->create();
       factory(App\StudentParent::class,10       )->create();
       factory(App\Student::class,10             )->create();
       factory(App\Paiment::class,10             )->create();
       factory(App\Note::class,10                )->create();
       factory(App\Complaint::class,10           )->create();
       factory(App\Delay::class,10               )->create();
       factory(App\Absent::class,10              )->create();
       factory(App\Teacher::class,10             )->create();
       factory(App\Employee::class,10            )->create();
       factory(App\TeacherSalary::class,10       )->create();
       factory(App\EmployeeSalary::class,10      )->create();
       factory(App\SchoolYear::class,10          )->create();
       factory(App\Subject::class,10             )->create();
       factory(App\Classroom::class,10           )->create();
       factory(App\Exam::class,10           )->create();
       factory(App\ExamResult::class,10           )->create();
       factory(App\ExamType::class,10           )->create();

    }
}
