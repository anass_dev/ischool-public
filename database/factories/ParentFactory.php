<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StudentParent;
use Faker\Generator as Faker;

use Illuminate\Support\Str;

$factory->define(StudentParent::class, function (Faker $faker) {
    return [
 

        'name'          =>  $faker->firstName(),
        'surname'       =>  $faker->lastName(),

        'cin'           =>  $faker->randomElement(['R', 'RB','G']).Str::random(10),
        'sex'           =>  $faker->randomElement(['m', 'f']),
        
        'phone_number'  =>  $faker->e164PhoneNumber(),
        'email'         =>  $faker->email(),

        'birthday'      =>  $faker->dateTimeBetween('-90 years','-30 years'),
        'address'       =>  $faker->address(),

    ];
});
