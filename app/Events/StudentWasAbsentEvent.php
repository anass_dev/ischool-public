<?php

namespace App\Events;

use App\Absent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StudentWasAbsentEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $absent;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Absent $absent)
    {
        $this->absent = $absent;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
