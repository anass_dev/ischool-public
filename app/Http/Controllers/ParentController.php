<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentParent;

class ParentController extends Controller
{


    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('parents.index')->with(["parents"=>StudentParent::all(['id','surname','name','email','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $parent = StudentParent::create($this->validateRequest());

        return redirect('parents/'.$parent->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(StudentParent $parent)
    {
        return view('parents.show')->with(["parent"=>$parent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentParent $parent)
    {
        
        return view('parents.edit')->with(["parent"=>$parent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentParent $parent)
    {
        
        $parent->update($this->validateRequest());

        return redirect('parents/'.$parent->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentParent $parent)
    {
        $parent->delete();

    }

    /**
     * 
     * 
     * 
     */
     private function validateRequest(){

         return request()->validate([

            'name'          =>'required|min:3|max:20',
            'surname'       =>'required|min:3|max:20',
            'cin'           =>'required',
            'sex'           =>'required|min:1|max:1',
            'phone_number'  =>'required|min:9|max:15',
            'email'         =>'required|email',
            'birthday'      =>'required|date',
            'address'       =>'required',
        ]);

     }
}
