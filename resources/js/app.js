/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

const axios = require('axios');

window.Vue = require('vue');



/*==== elements ==== */

Vue.component('breadcrumb'       , require('./components/elements/breadcrumb.vue').default);
Vue.component('side-menu'       , require('./components/elements/side-menu.vue').default);
Vue.component('am-table'       , require('./components/elements/table.vue').default);
Vue.component('small-table'       , require('./components/elements/small-table.vue').default);
Vue.component('modal-warning'       , require('./components/elements/modal-warning.vue').default);



Vue.component('secondary-button'       , require('./components/elements/secondary-button.vue').default);



// alerts:
Vue.component('alert-error'         , require('./components/elements/alert-error.vue').default);
Vue.component('alert-success'       , require('./components/elements/alert-success.vue').default);
Vue.component('alert-info'          , require('./components/elements/alert-info.vue').default);

// buttons:
Vue.component('back-button'         , require('./components/elements/back-button.vue').default);
Vue.component('save-button'         , require('./components/elements/save-button.vue').default);
Vue.component('success-button'      , require('./components/elements/success-button.vue').default);




/*==== forms ==== */

Vue.component('am-input'        , require('./components/forms/am-input.vue').default);
Vue.component('am-textarea'     , require('./components/forms/am-textarea.vue').default);
Vue.component('drop'            , require('./components/forms/drop.vue').default);
Vue.component('multi-drop'            , require('./components/forms/multi-drop.vue').default);
Vue.component('form-header'     , require('./components/forms/form-header.vue').default);
Vue.component('info-bar'        , require('./components/forms/info-bar.vue').default);
Vue.component('info-person-bar'        , require('./components/forms/info-person-bar.vue').default);
Vue.component('delete'        , require('./components/forms/delete.vue').default);




/*==== navs ==== */

Vue.component('am-profile'      , require('./components/navs/am-profile.vue').default);



/*==== Dashbord ==== */

Vue.component('card'        , require('./components/dashbord/card.vue').default);
Vue.component('info-card'   , require('./components/dashbord/info-card.vue').default);




/*==== Courses ==== */
Vue.component('classrooms-card'   , require('./components/courses/classrooms-card.vue').default);
Vue.component('teachers-card'   , require('./components/courses/teachers-card.vue').default);
Vue.component('subjects-card'   , require('./components/courses/subjects-card.vue').default);


const app = new Vue({
    el: '#app',
});
