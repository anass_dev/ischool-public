<?php

namespace App\Mail;

use App\Delay;
use App\StudentParent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentDelayedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $delay;
    public $subject;
    public $parent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Delay $delay,StudentParent $parent = null)
    {
        

        $this->delay    = $delay;
        
        $this->parent   = $parent;
        
        $student        = $delay->student;
        
        $this->subject  = "$student->name $student->surname was delayed";
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        
        if($this->parent)
        {   

            return $this->subject($this->subject)->markdown('mail.delayedStudentParent',[

                'delay'     => $this->delay,
                'parent'    => $this->parent
        
            ]);

        } else {
            return $this->subject($this->subject)->markdown('mail.delayedStudent',[

                'delay'     => $this->delay
        
            ]);
        }
            
    }
}
