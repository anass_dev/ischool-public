<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delay extends Model
{

    use SoftDeletes;
    protected $guarded = [];

    
    /**
     * create a many-to-one realtionship with students table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function student(){

        return $this->belongsTo('App\Student');
    }

    public static function message($student) {

        $params = Settings::params();

        if(trim($params->delays->email->message) != "") return trim($params->delays->email->message);

        return "Student " . $student->name . ' ' . $student->surname . ' is delayed today';  
        
    }
}
