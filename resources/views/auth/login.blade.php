@extends('layouts.auth')

@section('content')

<div class="w-full flex flex-wrap">


    <div class="w-1/2 ">
        <img class="object-cover w-full h-screen hidden md:block" 
        src="https://images.pexels.com/photos/714699/pexels-photo-714699.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940">
    </div>

    <!-- Login Section -->
    <div class="w-full md:w-1/2 flex flex-col">


        <div class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-24">
            <a href="{{ route('home') }}" class="bg-black text-white font-bold text-xl p-4">@include('layouts.main.svg-logo')</a>
        </div>



        <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
            
            <p class="text-center text-3xl">Welcome.</p>

          

            <form class="flex flex-col pt-3 md:pt-8"  method="POST" action="{{ route('login') }}" >

                @csrf



                <div class="flex flex-col pt-4">
                    
                    <label for="email" class="text-lg">{{ __('E-Mail Address') }}</label>
                    <input type="email" id="email" name="email" placeholder="your@email.com" class="appearance-none border rounded w-full p-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline @error('email') border-red-500 @enderror">
                    
                    @error('email')

                        <alert-error message="{{ $message  }}"></alert-error>

                    @enderror

                </div>




                <div class="flex flex-col pt-4">
                    
                    <label for="password" class="text-lg">{{ __('Password') }}</label>
                    <input type="password" id="password" name="password" placeholder="Password" class="appearance-none border rounded w-full p-3 text-gray-700 mb-3 leading-tight focus:outline-none @error('password') border-red-500 @enderror" >
                    
                    @error('password')
                        <alert-error message="{{ $message  }}"></alert-error>
                    @enderror
                
                </div>





                <div class="md:flex md:items-center pt-4">

                    <label class="block text-gray-500 font-bold">
                        <input class="form-checkbox text-black focus:shadow-none focus:border-orange-200" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="text-sm" for="remember">
                        {{ __('Remember Me') }}  
                        </span>
                    </label>

                </div>

            

                <input type="submit" value="{{ __('Login') }}" class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8">



            </form>
            @if (Route::has('password.request'))
            <div class="text-center pt-12 pb-12">
                <p>{{ __('Forgot Your Password?') }} <a href="{{ route('password.request') }}" class="underline font-semibold">click here.</a></p>
            </div>
            @endif
        </div>

    </div>

    <!-- Image Section -->
    
</div>

























<!--


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    
                        

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input  class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    
                                </div>
                            </div>
                        </div>


                        <div class="mt-6 flex items-center justify-between">
                            <div class="flex items-center">
                              <input id="remember_me" type="checkbox" class="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out" />
                              
                            </div>
                                
                          </div>

                            <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                    <span class="absolute left-0 inset-y-0 flex items-center pl-3">
                                      <svg class="h-5 w-5 text-indigo-500 group-hover:text-indigo-400 transition ease-in-out duration-150" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clip-rule="evenodd" />
                                      </svg>
                                    </span>
                                    
                            </button>
                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


-->
@endsection
