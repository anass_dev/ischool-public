<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\ExamType;

class ExamTypeControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_examtype(){

        $response = $this->get('/examtypes')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_examtype(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/examtypes')->assertStatus(200);

    }




    /** @test */
    public function create_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/examtypes/create')->assertStatus(200);

    }




    /** @test */
    public function view_show_is_not_accssible_examtype(){

       

        $this->actingAs(Factory(User::class)->create());
        $examtype = factory(ExamType::class)->create();

        $response = $this->get('/examtypes/'.$examtype->id)->assertStatus(405);

    }




    /** @test */
    public function a_examtype_can_be_added(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $response = $this->post('/examtypes',[

            'name'=> 'examtype__01',
            

        ]);

        $this->assertCount(1,ExamType::all());

    }




    /** @test */
    public function inserted_data_is_not_validated(){
            //$this->withoutExceptionHandling();
    
            $this->actingAs(Factory(User::class)->create());
    
            $response = $this->post('/examtypes',[
                'name'=> false,
    
            ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_examtype(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $examtype = factory(ExamType::class)->create();

        $response = $this->get('/examtypes/'.$examtype->id.'/edit')->assertStatus(200);

    }




    /** @test */
    public function a_examtype_can_be_updated(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $examtype = Factory(ExamType::class)->create();
    
        $response = $this->patch('/examtypes/'.$examtype->id,[
            
            'name'=> 'new_name_examtype_01',
            
        ]);

        $this->assertDatabaseHas('exam_types', ['name'=>'new_name_examtype_01']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $examtype = Factory(ExamType::class)->create();
    
        $this->patch('/examtypes/'.$examtype->id,[
            
            'name'=> '',
    
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('exam_types', ['name'=>'']);
    
    
    }




    /** @test */
    public function a_examtype_can_be_deleted(){

            //$this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());

        $examtype = Factory(ExamType::class)->create();
    
        $response = $this->delete('/examtypes/'.$examtype->id);

        $this->assertSoftDeleted('exam_types', ['id'=>$examtype->id]);
        
        
    }
}
