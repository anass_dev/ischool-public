@extends('layouts.app')
@section('content')


  
<div class="container p-2">
    
    <!-- ===== Breadcrumb ===== -->
        
    <breadcrumb :items="[

        {'name':'exam types', 'link':'{{ route('examtypes.index') }}' },
        {'name':'edit', 'link':'' }
    
    ]"></breadcrumb>

    

    <div class="border shadow-sm mt-4 rounded-md">

    
        <div class="bg-gray-100 rounded-md">

            <form-header title="{{_('Edit Exam Type') }}"></form-header>

        </div>  


    <form action="{{ route('examtypes.update',$type) }}" method="post">

            <div class="border-t border-b py-8 px-4">

                    @csrf
                    @method('PUT')
                    @include('exams.types.form')
                
            </div>

            <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                <back-button link="{{ route('examtypes.index') }}" text="{{ _('Back') }}"></back-button>
                <save-button text="Save"></save-button>
                
            </div>
    </form>

</div>


</div>


@endsection