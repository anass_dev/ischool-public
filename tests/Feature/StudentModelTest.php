<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Tests\TestCase;
use App;


class StudentModelTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * 
     * tests if the student correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_student_is_saved(){

        
        $student = factory(App\Student::class)->create();


        $student->save();

        $this->assertDatabaseHas('students', ['id'=>$student->id]);


    }   




    /**
     * 
     * tests if the student correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_student_is_updated(){
        
        $student = factory(App\Student::class)->create();
        
        $student->update(['email' => 'test@testemail.com']);
        
        $this->assertDatabaseHas('students', ['email' =>'test@testemail.com']);

    }




    /**
     * 
     * tests if the student correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_student_is_softDeleted(){

        $student = factory(App\Student::class)->create();

        $student->delete();

        $this->assertSoftDeleted($student);

    }




    /**
     * 
     * tests if the student correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_student_founde(){

        $student = factory(App\Student::class)->create();
        $student->save();


        $new_parent = App\Student::find($student->id);

        $this->assertEquals($student->id,$new_parent->id);

    }




    /**
     * 
     * tests the student  grade relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_grade_relationship(){

        $student = factory(App\Student::class)->create();

        $this->assertIsString($student->grade->name);


    }




    /**
     * 
     * tests the student parent relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_parent_relationship(){

        $student = factory(App\Student::class)->create();
        $this->assertIsString($student->parent->name);

    }




    /**
     * 
     * tests the student Absent relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_absent_relationship(){

        

        $student = factory(App\Student::class)->create();

        $absent = factory(App\Absent::class)->create();

        $absent->student_id = $student->id;

        $absent->save();


        $this->assertGreaterThan(0,$student->absents->count());

    }




    /**
     * 
     * tests the student delays relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_delay_relationship(){



        $student = factory(App\Student::class)->create();

        $absent = factory(App\Delay::class)->create();

        $absent->student_id = $student->id;

        $absent->save();


        $this->assertGreaterThan(0,$student->delays->count());

    }




    /**
     * 
     * tests the student notes relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_notes_relationship(){



        $student = factory(App\Student::class)->create();

        $note = factory(App\Note::class)->create();

        $note->student_id = $student->id;

        $note->save();


        $this->assertGreaterThan(0,$student->notes->count());

    }




    /**
     * 
     * tests the student notes relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_complaint_relationship(){



        $student = factory(App\Student::class)->create();

        $complaint = factory(App\Complaint::class)->create();

        $complaint->student_id = $student->id;

        $complaint->save();


        $this->assertGreaterThan(0,$student->complaints->count());

    }




    /**
     * 
     * tests the student paiment relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_paiment_relationship(){



        $student = factory(App\Student::class)->create();

        $paiment = factory(App\Paiment::class)->create();

        $paiment->student_id = $student->id;

        $paiment->save();


        $this->assertGreaterThan(0,$student->paiments->count());

    }




    /**
     * 
     * tests the student debts relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_student_debts_relationship(){



        $student = factory(App\Student::class)->create();

        $debt = factory(App\Debt::class)->create();

        $debt->student_id = $student->id;

        $debt->save();


        $this->assertGreaterThan(0,$student->debts->count());

    }

}
