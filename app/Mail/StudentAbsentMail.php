<?php

namespace App\Mail;

use App\Absent;
use App\StudentParent;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentAbsentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $absent;
    public $subject;
    public $parent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Absent $absent,StudentParent $parent = null)
    {
        $this->absent    = $absent;
        $this->parent   = $parent;
        
        $student        = $absent->student;
        $this->subject  = "$student->name $student->surname was absent";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        if($this->parent)
        {   

            return $this->subject($this->subject)->markdown('mail.absentStudentParent',[

                'absent'     => $this->absent,
                'parent'    =>  $this->parent
        
            ]);

        } else {
            return $this->subject($this->subject)->markdown('mail.absentStudent',[

                'absent'     => $this->absent
        
            ]);
        }
    }
}
