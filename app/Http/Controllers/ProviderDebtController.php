<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Provider;
use App\ProviderDebt as Debt;

class ProviderDebtController extends Controller
{
    
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Provider $provider)
    {
        return view('providers.debts.index')->with(["debts"=>$provider->debts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Provider $provider)
    {
        return view('providers.debts.create')->with(["provider"=>$provider]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Provider $provider)
    {
        $debt  = Debt::make($this->validateRequest());
        $debt->provider_id = $provider->id;
        $debt->save();

        return redirect('/providers/'.$provider->id.'/absents');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider,Debt $debt)
    {
        return view('providers.debts.edit')->with(["debt"=>$debt,"provider"=>$provider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Provider $provider,Debt $debt)
    {
        $debt->update($this->validateRequest());

        return redirect('/providers/'.$provider->id.'/debts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider,Debt $debt)
    {
        $debt->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'          => 'required|max:100',
            'amount'        => 'required|numeric',
        ]);


    } 

}
