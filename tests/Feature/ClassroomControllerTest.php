<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Classroom;


class ClassroomControllerTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function non_login_user_can_not_access_controller_classroom(){

        $response = $this->get('/classrooms')->assertRedirect('/login');

    }




    /** @test */
    public function login_user_can_access_controller_classroom(){

       

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/classrooms')->assertOk();

    }




    /** @test */
    public function show_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());

        $response = $this->get('/classrooms/create')->assertOk();

    }




    /** @test */
    public function classroom_view_create_is_accssible(){

       

        $this->actingAs(Factory(User::class)->create());
        $classroom = factory(Classroom::class)->create();

        $response = $this->get('/classrooms/'.$classroom->id)->assertOk();

    }




    /** @test */
    public function classroom_can_be_added(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        
        $this->post('/classrooms',[
            'capacity'  => 15,
            'name'      => 'e210',
            'occupied'  => false,
        ]);

        $this->assertCount(1,Classroom::all());
    }




    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());

        $this->post('/classrooms',[
            'name'      => '10',
            'occupied'  => 'else',

        ])->assertSessionHasErrors();
    
    
    }




    /** @test */
    public function view_update_is_accssible_classroom(){

       

        $this->actingAs(Factory(User::class)->create());

        $classroom = factory(Classroom::class)->create();

        $response = $this->get('/classrooms/'.$classroom->id.'/edit');

        $response->assertStatus(200);

    }




    /** @test */
    public function a_classroom_can_be_updated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $classroom = Factory(Classroom::class)->create();
        
       

        $this->patch('/classrooms/'.$classroom->id,[
            'capacity'  => 20,
            'name'      => 'new_name',
            'occupied'  => false,
        ]);

        $this->assertDatabaseHas('classrooms', ['name'=>'new_name']);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $classroom = Factory(Classroom::class)->create();
    
        $this->patch('/classrooms/'.$classroom->id,[

            'capacity'  => 25440,
            'name'      => 'sqd',
            'occupied'  => false,
    
        ])->assertSessionHasErrors();

        $this->assertDatabaseMissing('classrooms', ['name'=>'sqd']);

    
    }




    /** @test */
    public function a_classroom_can_be_deleted(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());
        $classroom = Factory(Classroom::class)->create();

    
        $this->delete('/classrooms/'.$classroom->id);
        $this->assertSoftDeleted('classrooms', ['id'=>$classroom->id]);
        
        
    }
}
