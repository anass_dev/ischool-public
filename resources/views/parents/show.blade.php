@extends('layouts.app')
@section('content')

<div class="pt-2 px-4">
                    <!-- ===== Breadcrumb ===== -->
                
                    <breadcrumb :items="[

                        {'name':'parents', 'link':'{{ route('parents.index') }}' }, 
                        {'name':'{{ $parent->name . ' ' . $parent->surname }}', 'link':'' }
                    
            ]"></breadcrumb>
</div>

<div class="container p-2">
    

    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-5 rounded-md shadow-md">

                
                <div class="mt-2 flex flex-col items-center justify-center">
                    <div class="text-center">
                        <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26">
                            <path d="M10.119 16.064c2.293-.53 4.427-.994 3.394-2.946-3.147-5.941-.835-9.118 2.488-9.118 3.388 0 5.643 3.299 2.488 9.119-1.065 1.964 1.149 2.427 3.393 2.946 1.985.458 2.118 1.428 2.118 3.107l-.003.828h-1.329c0-2.089.083-2.367-1.226-2.669-1.901-.438-3.695-.852-4.351-2.304-.239-.53-.395-1.402.226-2.543 1.372-2.532 1.719-4.726.949-6.017-.902-1.517-3.617-1.509-4.512-.022-.768 1.273-.426 3.479.936 6.05.607 1.146.447 2.016.206 2.543-.66 1.445-2.472 1.863-4.39 2.305-1.252.29-1.172.588-1.172 2.657h-1.331c0-2.196-.176-3.406 2.116-3.936zm-10.117 3.936h1.329c0-1.918-.186-1.385 1.824-1.973 1.014-.295 1.91-.723 2.316-1.612.212-.463.355-1.22-.162-2.197-.952-1.798-1.219-3.374-.712-4.215.547-.909 2.27-.908 2.819.015.935 1.567-.793 3.982-1.02 4.982h1.396c.44-1 1.206-2.208 1.206-3.9 0-2.01-1.312-3.1-2.998-3.1-2.493 0-4.227 2.383-1.866 6.839.774 1.464-.826 1.812-2.545 2.209-1.49.345-1.589 1.072-1.589 2.334l.002.618z"/>
                        </svg>
                    </div>

                    <h2 class="p-2 text-xl text-center text-gray-700">{{ $parent->name . ' ' . $parent->surname }}</h2>
                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-address-card"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $parent->cin }}</div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-phone"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $parent->phone_number }}</div>

                </div>

                <div class="mt-2 flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-at"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $parent->email }} </div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-calendar"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $parent->birthday }} </div>

                </div>

                <div class="flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-map"></i>  </label>
                    <textarea class="p-2 text-sm text-gray-700" cols="30" rows="3" readonly>{{ $parent->address }}</textarea>
                </div>
            </div>

            </div>

            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">
                <div class="m-2 p-5 rounded-md shadow-md">
                    <div class="flex items-center justify-between">
                    <h2 class="text-gray-600 text-xl py-5 w-1/2"> Students </h2>
                        <div>
                            <success-button text="{{ _('Add student') }}" link="{{ route('students.create') }}"></success-button>
                        </div>
                    </div>
                    
                    <small-table 
                        :items="{{ $parent->students()->get(['id','name','created_at']) }}" 
                        index_link="{{ route('students.index') }}" ></small-table>

                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection