<?php

namespace Tests\Feature;


use App;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;


class TeacherTest extends TestCase
{
    use RefreshDatabase;



    /**
     * 
     * tests if the teacher correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_teacher_is_saved(){

        
        $teacher = factory(App\Teacher::class)->create();

        $teacher->save();

        $this->assertDatabaseHas('teachers', ['id'=>$teacher->id]);


    }   




    /**
     * 
     * tests if the teacher correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_teacher_is_updated(){
        
        $teacher = factory(App\Teacher::class)->create();
        
        $teacher->update(['email' => 'test@testemail.com']);
        
        $this->assertDatabaseHas('teachers', ['email' =>'test@testemail.com']);

    }




    /**
     * 
     * tests if the teacher correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_teacher_is_softDeleted(){

        $teacher = factory(App\Teacher::class)->create();

        $teacher->delete();

        $this->assertSoftDeleted($teacher);

    }




    /**
     * 
     * tests if the teacher correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_teacher_founde(){

        $teacher = factory(App\Teacher::class)->create();
        $teacher->save();


        $new_parent = App\Teacher::find($teacher->id);

        $this->assertEquals($teacher->id,$new_parent->id);

    }



    
    /**
     * 
     * tests the teacher Absent relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_current_salary_relationship(){

        
        $salary  = factory(App\TeacherSalary::class)->create();


        $teacher = App\Teacher::find($salary->teacher_id);

        $teacher->current_salary = $salary->id;


        $teacher->save();

     

        $this->assertEquals($salary->amount,$teacher->salary->amount);

    }


    /**
     * 
     * tests the teacher Salary relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_teacher_salarys_relationship(){

        
        $salary = factory(App\TeacherSalary::class)->create();
        $teacher = App\Teacher::find($salary->teacher_id);


        $this->assertGreaterThan(0,$teacher->salaries->count());

    }
    

}
