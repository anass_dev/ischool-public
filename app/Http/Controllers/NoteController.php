<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Note;
use App\Student;


class NoteController extends Controller
{

    /**
     * 
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $student)
    {
        return view('students.notes.index')->with(['student'=>$student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $student)
    {
        return view('students.notes.create')->with(['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $student)
    {
        $note  = Note::make($this->validateRequest());
        $note->student_id = $student->id;
        $note->save();

        return redirect('/students/'.$student->id.'/notes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,Note $note)
    {
        return view('students.notes.edit')->with(['student'=>$student,"note"=>$note]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Student $student,Note $note)
    {
        $note->update($this->validateRequest());

        return redirect('/students/'.$student->id.'/notes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student,Note $note)
    {
        $note->delete();
    }

    /**
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([
            'name'          => 'required|max:100',
            'description'   => 'nullable',
        ]);

    } 
}
