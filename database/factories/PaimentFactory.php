<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Paiment;
use Faker\Generator as Faker;
use App\Student;


$factory->define(Paiment::class, function (Faker $faker) {
    return [
        
        'name'          =>  $faker->word(),
        'method'        =>  $faker->randomElement(['cash', 'chique']),
        'amount'        =>  $faker->randomFloat(2,1500,4500),
        'status'        =>  $faker->randomElement(['confirmed', 'notconfirmed']),
        'student_id'    =>  Student::find(1)->id

    ];
});
