<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App;

class DebtTest extends TestCase
{
    use RefreshDatabase;


    
    /**
     * 
     * tests if the Debt correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_Debt_is_saved(){

        
        $Debt = factory(App\Debt::class)->create();


        $Debt->save();

        $this->assertDatabaseHas('debts', ['id'=>$Debt->id]);


    }
    




    /**
     * 
     * tests if the Debt correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_Debt_is_updated(){
        
        $Debt = factory(App\Debt::class)->create();
        
        $Debt->update(['amount' => 15131]);
        
        $this->assertDatabaseHas('debts', ['amount' =>15131]);

    }




    /**
     * 
     * tests if the Debt correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_Debt_is_softDeleted(){

        $Debt = factory(App\Debt::class)->create();

        $Debt->delete();

        $this->assertSoftDeleted($Debt);

    }




    /**
     * 
     * tests if the Debt correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_Debt_founde(){

        $Debt = factory(App\Debt::class)->create();
        $Debt->save();


        $new_parent = App\Debt::find($Debt->id);

        $this->assertEquals($Debt->id,$new_parent->id);

    }




    /**
     * 
     * tests the Debt Debts relationship is working
     * 
     * @return void
     * 
     */

     
    public function test_Debt_student_relationship(){

        $Debt = factory(App\Debt::class)->create();

        $student = factory(App\Student::class)->create();

        $Debt->student_id = $student->id;

        $Debt->save();


        $this->assertEquals($student->id,$Debt->student->id);


    }
}
