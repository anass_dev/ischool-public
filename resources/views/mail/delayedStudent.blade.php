@component('mail::message')

Hi {{ $delay->student->name }} {{ $delay->student->surname }},

we notify you that a delay is mark for {{ $delay->duration }}mins.

@if(trim($delay->description) != '')
more information:<br>
{{ $delay->description }}
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent