@extends('layouts.app')
@section('content')

<div class="pt-2 px-4">
 <!-- ===== Breadcrumb ===== -->
                
 <breadcrumb :items="[

    {'name':'teachers', 'link':'{{ route('teachers.index') }}' }, 
    {'name':'{{ $teacher->name . ' ' . $teacher->surname }}', 'link':'' }

]"></breadcrumb>

</div>

<div class="container p-2">
    

    <div>
        <div class="flex flex-wrap">
            
            <div class="flex-none sm:w-full md:w-1/3 xl:w-1/4" >
                <div class="m-2 p-5 rounded-md shadow-md">

                
                <div class="mt-2 flex flex-col items-center justify-center">
                    <div class="text-center">
                        <svg class="fill-current text-gray-400 w-1/4 m-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                            <path d="M6 3.447h-1v-1.447h19v16h-7.731l2.731 4h-1.311l-2.736-4h-1.953l-2.736 4h-1.264l2.732-4h-2.732v-1h8v-1h3v1h3v-14h-17v.447zm2.242 17.343c-.025.679-.576 1.21-1.256 1.21-.64 0-1.179-.497-1.254-1.156l-.406-4.034-.317 4.019c-.051.656-.604 1.171-1.257 1.171-.681 0-1.235-.531-1.262-1.21l-.262-6.456-.308.555c-.241.437-.8.638-1.265.459-.404-.156-.655-.538-.655-.951 0-.093.012-.188.039-.283l1.134-4.098c.17-.601.725-1.021 1.351-1.021h4.096c.511 0 1.012-.178 1.285-.33.723-.403 2.439-1.369 3.136-1.793.394-.243.949-.147 1.24.217.32.396.286.95-.074 1.297l-3.048 2.906c-.375.359-.595.849-.617 1.381-.061 1.397-.3 8.117-.3 8.117zm-5.718-10.795c-.18 0-.34.121-.389.294-.295 1.04-1.011 3.666-1.134 4.098l1.511-2.593c.172-.295.623-.18.636.158l.341 8.797c.01.278.5.287.523.002 0 0 .269-3.35.308-3.944.041-.599.449-1.017.992-1.017.547.002.968.415 1.029 1.004.036.349.327 3.419.385 3.938.043.378.505.326.517.022 0 0 .239-6.725.3-8.124.033-.791.362-1.523.925-2.061l3.045-2.904c-.661.492-2.393 1.468-3.121 1.873-.396.221-1.07.457-1.772.457h-4.096zm16.476 1.005h-5v-1h5v1zm2-2h-7v-1h7v1zm-15.727-4.994c-1.278 0-2.315 1.038-2.315 2.316 0 1.278 1.037 2.316 2.315 2.316s2.316-1.038 2.316-2.316c0-1.278-1.038-2.316-2.316-2.316zm0 1c.726 0 1.316.59 1.316 1.316 0 .726-.59 1.316-1.316 1.316-.725 0-1.315-.59-1.315-1.316 0-.726.59-1.316 1.315-1.316zm15.727 1.994h-7v-1h7v1z"/>
                        </svg>
                    </div>

                    <h2 class="p-2 text-xl text-center text-gray-700">{{ $teacher->name . ' ' . $teacher->surname }}</h2>
                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-address-card"></i>  </label>
                    <div class="p-2 text-sm text-sm text-gray-700"> {{ $teacher->cin }}</div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-phone"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $teacher->phone_number }}</div>

                </div>

                <div class="mt-2 flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-at"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $teacher->email }} </div>

                </div>

                <div class="mt-2 flex  items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-calendar"></i>  </label>
                    <div class="p-2 text-sm text-gray-700"> {{ $teacher->birthday }} </div>

                </div>

                <div class="flex items-start">
                    <label class="p-2 text-blue-400"><i class="fas fa-map"></i>  </label>
                    <textarea class="p-2 text-sm text-gray-700" cols="30" rows="3" readonly>{{ $teacher->address }}</textarea>
                </div>
            </div>

            </div>

            
            <div class="sm:flex-none md:flex-auto sm:w-full md:w-1/3 xl:2/4 ">
                <div class="m-2 p-5 rounded-md shadow-md">
                    
                    <div class="flex items-center justify-between">
                        <div class="flex items-center justify-start">
                            <svg class="fill-current text-gray-400 w-8 mx-2 mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26" fill-rule="evenodd" clip-rule="evenodd">
                                <path d="M12.164 7.165c-1.15.191-1.702 1.233-1.231 2.328.498 1.155 1.921 1.895 3.094 1.603 1.039-.257 1.519-1.252 1.069-2.295-.471-1.095-1.784-1.827-2.932-1.636zm1.484 2.998l.104.229-.219.045-.097-.219c-.226.041-.482.035-.719-.027l-.065-.387c.195.03.438.058.623.02l.125-.041c.221-.109.152-.387-.176-.453-.245-.054-.893-.014-1.135-.552-.136-.304-.035-.621.356-.766l-.108-.239.217-.045.104.229c.159-.026.345-.036.563-.017l.087.383c-.17-.021-.353-.041-.512-.008l-.06.016c-.309.082-.21.375.064.446.453.105.994.139 1.208.612.173.385-.028.648-.36.774zm10.312 1.057l-3.766-8.22c-6.178 4.004-13.007-.318-17.951 4.454l3.765 8.22c5.298-4.492 12.519-.238 17.952-4.454zm-2.803-1.852c-.375.521-.653 1.117-.819 1.741-3.593 1.094-7.891-.201-12.018 1.241-.667-.354-1.503-.576-2.189-.556l-1.135-2.487c.432-.525.772-1.325.918-2.094 3.399-1.226 7.652.155 12.198-1.401.521.346 1.13.597 1.73.721l1.315 2.835zm2.843 5.642c-6.857 3.941-12.399-1.424-19.5 5.99l-4.5-9.97 1.402-1.463 3.807 8.406-.002.007c7.445-5.595 11.195-1.176 18.109-4.563.294.648.565 1.332.684 1.593z"/>
                            </svg>
                            <h2 class="text-gray-600 text-xl py-5 w-1/2"><a href="{{ route('teachers.salaries.index', ['teacher' => $teacher->id]) }}"> Salary History </a></h2>
                            
                        </div>
                            <div>
                                <success-button text="{{ _('Add salary') }}" link="{{ route('teachers.salaries.create', ['teacher' => $teacher->id]) }}"></success-button>
                            </div>
                    </div>
                    
                    
                    <small-table 
                        :items="{{ $teacher->salaries()->get(['id','amount','created_at']) }}" 
                        index_link="{{ route('teachers.salaries.index', ['teacher' => $teacher->id]) }}" ></small-table>
        

                   
                </div>
                
            </div>

        </div>

    </div>
</div>

@endsection