<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <head>

        @include('layouts.partials.header')

    </head>

    <body >

        <div id="app">
            
            @yield('content')

        </div>



        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>


    </body>
</html>
