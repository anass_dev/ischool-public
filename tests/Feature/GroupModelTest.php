<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App;

class GroupTest extends TestCase
{
    
    use RefreshDatabase;



    /**
     * 
     * tests if the group correctly inserted in a database
     * 
     * @return void
     * 
     */

    public function test_group_is_saved(){

        
        $group = factory(App\Group::class)->create();

        $group->save();

        $this->assertDatabaseHas('groups', ['id'=>$group->id]);


    }   




    /**
     * 
     * tests if the group correctly updated in a database
     * 
     * @return void
     * 
     */


    public function test_group_is_updated(){
        
        $group = factory(App\Group::class)->create();
        
        $group->update(['name' => 'special_group_name']);
        
        $this->assertDatabaseHas('groups', ['name' => 'special_group_name']);

    }




    /**
     * 
     * tests if the group correctly soft deleted in a database
     * 
     * @return void
     * 
     */

     
    public function test_group_is_softDeleted(){

        $group = factory(App\Group::class)->create();

        $group->delete();

        $this->assertSoftDeleted($group);

    }




    /**
     * 
     * tests if the group correctly founded from  database
     * 
     * @return void
     * 
     */

     
    public function test_group_founde(){

        $group = factory(App\Group::class)->create();
        $group->save();


        $new_parent = App\Group::find($group->id);

        $this->assertEquals($group->id,$new_parent->id);

    }



    
    /**
     * 
     * tests the group Student relationship atachment
     * 
     * @return void
     * 
     */

     
    public function test_group_student_relationship(){

        
        $students  = factory(App\Student::class,5)->create();
        $group  = factory(App\Group::class)->create();


        $group->students()->attach($students);

        $affected_students  = App\StudentGroup::where(['group_id'=> $group->id])->get();

        $this->assertEquals($affected_students->count(),$students->count());

    }

}
