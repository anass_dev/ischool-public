<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;


class TeacherController extends Controller
{
    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teachers.index')->with(['teachers'=>Teacher::all(['id','surname','name','email','created_at','updated_at'])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Teacher $teacher)
    {
        $teacher = teacher::create($this->validateRequest());

        return redirect('teachers/'.$teacher->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return view('teachers.show')->with( ['teacher'=>$teacher]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.edit')->with( ['teacher'=>$teacher ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Teacher $teacher)
    {
        $teacher->update($this->validateRequest());
        return redirect('teachers/'.$teacher->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $teacher->delete();
    }

    /**
     * 
     * 
     * 
     */
    private function validateRequest(){

        return request()->validate([

           'name'          =>'required|min:3|max:20',
           'surname'       =>'required|min:3|max:20',
           'cin'           =>'required',
           'sex'           =>'required|min:1|max:1',
           'phone_number'  =>'required|min:9|max:15',
           'email'         =>'required|email',
           'birthday'      =>'required|date',
           'address'       =>'required',
       ]);

    }
}
