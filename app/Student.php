<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{   
    use SoftDeletes;
    use Notifiable;

    protected $guarded = [];
    
    /**
     * Route notifications for the Nexmo channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        //return $this->phone_number;

        return '212666673732';
    }

    /**
     * create a one-to-one realtionship with parents table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */

    public function parent(){

        return $this->belongsTo('App\StudentParent');
    }




    /**
     * create a one-to-one realtionship with grade table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */

    public function grade(){

        return $this->belongsTo('App\Grade');
    }




    /**
     * create a one-to-many realtionship with absents table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function absents(){

        return $this->hasMany('App\Absent');
    }

    


    /**
     * create a one-to-many realtionship with delays table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function delays(){

        return $this->hasMany('App\Delay');
    }




    /**
     * create a one-to-many realtionship with notes table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function notes(){

        return $this->hasMany('App\Note');
    }

    


    /**
     * create a one-to-many realtionship with complaints table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function complaints(){

        return $this->hasMany('App\Complaint');
    }


    

    /**
     * create a one-to-many realtionship with paiments table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function paiments(){

        return $this->hasMany('App\Paiment');
    }

    



    /**
     * create a one-to-many realtionship with debts table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function debts(){

        return $this->hasMany('App\Debt');
    }




    /**
     * create a one-to-many realtionship with exam_results table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function ExamResults(){

        return $this->hasMany('App\ExamResult');
    }


    /**
     * The roles that belong to the user.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group', 'student_group')->withTimestamps();
    }

    


}

