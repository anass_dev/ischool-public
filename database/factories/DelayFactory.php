<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Delay;
use App\Student;

use Faker\Generator as Faker;

$factory->define(Delay::class, function (Faker $faker) {
    return [
        
        'name'          =>  $faker->word(),
        'resolved'      =>  $faker->boolean($chanceOfGettingTrue = 70),
        'description'   =>  $faker->text(100),
        'duration'      =>  $faker->numberBetween(1, 30),
        'student_id'    =>  factory(Student::class)
        
    ];
});
