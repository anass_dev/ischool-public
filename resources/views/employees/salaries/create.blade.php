@extends('layouts.app')
@section('content')

<div class="p-2">
    <!-- ===== Breadcrumb ===== -->
    
    <breadcrumb :items="[

        {'name':'{{ $employee->name.' '.$employee->surname }}', 'link':'{{ route('employees.show',$employee) }}' }, 
        {'name':'salaries', 'link':'{{ route('employees.salaries.index',$employee) }}' },
        {'name':'add', 'link':'' }
    
    ]"></breadcrumb>
</div>

<div class="container p-2">
     
    <div class="border shadow-sm rounded-md">

    
            <div class="bg-gray-100 rounded-md">

                <form-header title="{{_('Add Salary') }}"></form-header>
                <info-person-bar :person="{{ $employee }}"></info-person-bar>

            </div>  

            <form action="{{ route('employees.salaries.store',$employee) }}" method="post">

                    <div class="border-t border-b py-8 px-4">

                            @csrf
                            
                            @include('employees.salaries.form')
                        
                    </div>

                    <div class="py-6 px-5 bg-gray-100 rounded-md text-right flex justify-end">

                        <back-button link="{{ route('employees.salaries.index',$employee) }}" text="{{ _('Back') }}"></back-button>
                        <save-button text="Save"></save-button>
                        
                    </div>
            </form>

        </div>
    
        
    </div>

@endsection