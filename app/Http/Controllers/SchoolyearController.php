<?php

namespace App\Http\Controllers;

use App\SchoolYear;
use Illuminate\Http\Request;

class SchoolyearController extends Controller
{

    /**
     * 
     * 
     * 
     */
    public function __construct()
    {
            $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school_years.index')->with(['school_year'=>SchoolYear::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school_years.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        SchoolYear::create($this->requestValidate());
        return redirect('/schoolyears');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolYear $schoolyear)
    {
        return view('school_years.show')->with(['school_year'=>$schoolyear]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolYear $schoolyear)
    {
        return view('school_years.edit')->with(['school_year'=>$schoolyear]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolYear $schoolyear)
    {
       $schoolyear->update($this->requestValidate());
       return redirect('/schoolyears/'.$schoolyear->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolYear $schoolyear)
    {
        $schoolyear->delete();
    }

    /**
     * 
     * 
     */
    private function requestValidate(){
        return request()->validate([
            'name'      =>  'required|max:80',
            'first_day' =>  'required|date',
            'last_day'  =>  'required|date',
        ]);
    }
}
