<?php

namespace Tests\Feature;

use App\Exam;
use App\ExamResult;
use App\Student;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExamResultControllerTest extends TestCase
{
   use RefreshDatabase;

   /** @test */
   public function non_login_user_can_not_see_results(){

        $exam = factory(Exam::class)->create();
       $this->get('/exams/'.$exam->id.'/results')->assertRedirect('login');

   }




   /** @test */
    public function login_user_can_see_results(){

        $this->actingAs(factory(User::class)->create());

        $exam = factory(Exam::class)->create();
        $this->get('/exams/'.$exam->id.'/results')->assertStatus(200);

    }



    /** @test */
    public function create_view_is_accessible(){

        $this->actingAs(Factory(User::class)->create());
        $exam = factory(Exam::class)->create();

        $this->get('/exams/'.$exam->id.'/results/create')->assertStatus(200);
    
    }



    
    /** @test */
    public function single_result_show_view_is_not_accssible(){

    
        $this->actingAs(Factory(User::class)->create());

        $result = factory(ExamResult::class)->create();
        $exam   = factory(Exam::class)->create();

        $this->get('/exams/'.$exam->id.'/results/'.$result->id)->assertStatus(405);

    }



    /** @test */
    public function exam_result_can_be_added(){

        $this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());


        $exam       = factory(Exam::class)->create();
        $student    = factory(Student::class)->create();

        $this->post('/exams/'.$exam->id.'/results',[
    
                'student_id'    => $student->id,
                'mark'          => 12,
                
        ])->assertRedirect('/exams/'.$exam->id.'/results');
    
        $this->assertCount(1,ExamResult::all());
    
    }





    /** @test */
    public function inserted_data_is_not_validated(){

        //$this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());


        $exam       = factory(Exam::class)->create();


        $this->post('/exams/'.$exam->id.'/results',[
            'student_id'    => false,
            'mark'          => 120,

        ])->assertSessionHasErrors();


    }




    /** @test */
    public function view_update_is_accssible_examtype(){

        $this->withoutExceptionHandling();

        $this->actingAs(Factory(User::class)->create());



        $result = factory(ExamResult::class)->create();


        $this->get('/exams/'.$result->exam_id.'/results/'.$result->id.'/edit')->assertStatus(200);

    
    }
    



    /** @test */
    public function result_is_updated(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $result = Factory(ExamResult::class)->create();
    
        $response = $this->patch('/exams/'.$result->exam_id.'/results/'.$result->id,[
            
            'mark'          => 19,
            'student_id'    => $result->student_id
            
        ]);

        $this->assertDatabaseHas('exam_results', ['mark'=>19]);
    
    
    }




    /** @test */
    public function updated_data_is_not_valide(){

        //$this->withoutExceptionHandling();
    
        $this->actingAs(Factory(User::class)->create());

        $result = Factory(ExamResult::class)->create();
    
        $this->patch('/exams/'.$result->exam_id.'/results/'.$result->id,[
            
            'mark'=> '1ds',
    
    
        ])->assertSessionHasErrors();
    
    }


        /** @test */
    public function a_examtype_can_be_deleted(){

        $this->withoutExceptionHandling();
        
        $this->actingAs(Factory(User::class)->create());


        $result = Factory(ExamResult::class)->create();

    
        $this->delete('/exams/'.$result->exam_id.'/results/'.$result->id);


        $this->assertSoftDeleted('exam_results', ['id'=>$result->id]);
        
        
    }

}
